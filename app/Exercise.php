<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $table = 't_exercise';

    protected $fillable = [
        'review',
        'publish'
    ];

    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function answer()
    {
        return $this->belongsTo('App\Answer', 'answer_id');
    }
}
