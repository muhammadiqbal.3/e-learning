<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 't_answer';

    protected $fillable = [
        'answer',
        'correct'
    ];

    public function exercise()
    {
        return $this->hasMany('App\Exercise', 'answer_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }
}
