<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table = 't_lesson';

    protected $fillable = [
        'lesson_name',
        'lesson_thumbnail',
        'lesson_video',
        'lesson_detail',
        'status'
    ];

    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }

    public function question()
    {
        return $this->hasMany('App\Question', 'lesson_id');
    }
}
