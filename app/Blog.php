<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 't_blog';

    protected $fillable = [
        'blog_name',
        'blog_thumbnail',
        'blog_detail'
    ];

}
