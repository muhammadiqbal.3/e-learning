<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 't_course';

    protected $fillable = [
        'course_name',
        'course_thumbnail',
        'course_detail'
    ];

    public function lesson()
    {
        return $this->hasMany('App\Lesson');
    }
}
