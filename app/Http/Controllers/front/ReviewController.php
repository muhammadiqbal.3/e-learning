<?php

namespace App\Http\Controllers\front;

use App\Lesson;
use App\Exercise;
use App\Question;
use App\Answer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function index($lesson_id, Request $request)
    {
        $lesson = DB::table('t_lesson')
                    ->where('id', $lesson_id)            
                    ->first();
        $question = DB::table('t_question')
                    ->where('lesson_id', $lesson_id)
                    ->get();
        $id_question = $request->input('question_id');
        
        // $jawaban = array();
        $score_akhir = 0;
        $question_true = 0;
        $quest = 0;
        $index = 0;
        foreach($id_question as $q){
            $answer = DB::table('t_answer')->where('question_id', $q)
                    ->get();
            $i=1;
           foreach($answer as $ans){
               if($request->input('question_input'.($quest+$i)) == "true" && $ans->correct == "true"){
                $score_akhir += 20;
                $question_true += 1;
               }
               $i++;
           }
            $index++;
            $i -= 1;
            $quest = $index*$i;
        }
        $jawaban_user = array();
        for ($i=1; $i <= 25; $i++) { 
            $jawaban_user[$i] = $request->input('question_input'.$i);
        }

        return view('content.front.practice.review')
        ->with('page_title', $lesson->lesson_name)        
        ->with('score_akhir', $score_akhir)
        ->with('question_true', $question_true)
        ->with('jawaban_user', $jawaban_user)
        ->with('lesson', $lesson)       
        ->with('question', $question);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function show(Exercise $exercise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function edit(Exercise $exercise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exercise $exercise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exercise $exercise)
    {
        //
    }
}
