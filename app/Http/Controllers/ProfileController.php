<?php

namespace App\Http\Controllers;

use Auth;
use Storage;
use Carbon\Carbon;
use Hash;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = DB::table('users')->get();
        if(auth()->user()->role != 'student'){
            return view('content.dashboard.profile.index', compact('user'));
        }
        else{
            return view('content.front.profile.index', compact('user'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'email', 'max:191', 'unique:users,email,'.$id],
            'username' => ['required', 'string', 'min:3', 'max:191', 'unique:users,username,'.$id],
            'photo' => ['nullable', 'mimes:jpg,jpeg,png'],
            'gender' => ['nullable'],
            'institute' => ['required', 'string', 'max:191']
        ]);

        $user = User::findOrFail($id);
        if($request->email != $user->email){
            $user->email_verified_at = null;
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;

        if($request->hasFile('photo')){
            $uploadedFile = $request->file('photo');        
            $filename = 'avatar-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/user_photo/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $user->photo = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }
        
        $user->gender = $request->gender;
        $user->institute = $request->institute;
        
        $user->save();

        return redirect()->route('profile')->with([
            'status' => 'success',
            'message' => 'Ubah Profil Berhasil'
        ]);
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:5', 'max:191'],
            'old_password' => ['required'],
            'password_confirmation' => ['required'],
        ]);
      
        if(!Hash::check($request->old_password, Auth::user()->password)){
            return response()->json([
                'errors' => ['old_password' => 'Password lama tidak cocok']
            ], 422);
        } else if(Hash::check($request->password, Auth::user()->password)){
            return response()->json([
                'errors' => ['password' => 'Tidak dapat menggunakan password sebagai password baru']
            ], 422);
        }

        $user = User::findOrFail($id);
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Ubah Password berhasil'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
