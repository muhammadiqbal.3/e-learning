<?php

namespace App\Http\Controllers\dashboard;

use Storage;
use Carbon\Carbon;
use App\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class BlogController extends Controller
{
    protected $file_location = 'public/blog';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = DB::table('t_blog')->get();
        return view('content.dashboard.blog.index', compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.dashboard.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'blog_name' => ['required', 'string', 'max:191'],
            'blog_detail' => ['required', 'string'],
            'blog_thumbnail' => ['nullable', 'mimes:jpg,jpeg,png'],
        ]);

        $blog = new Blog;
        $blog->blog_name = $request->blog_name;
        $blog->blog_detail = $request->blog_detail;

        if($request->hasFile('blog_thumbnail')){
            $uploadedFile = $request->file('blog_thumbnail');        
            $filename = 'blog-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/gambar_blog/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $blog->blog_thumbnail = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }

        $blog->save();

        return redirect()->route('dashboard.blog.index')->with([
            'status' => 'success',
            'message' => 'Blog successfully added'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $blog = Blog::findOrFail($id);
        // $blog = Blog::whereBlogName($id)->first();
        $blog = DB::table('t_blog')->whereBlogName($id)->first();
        return view('content.dashboard.blog.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        // $blog = Blog::findOrFail($id);

        return view('content.dashboard.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        // $blog = Blog::findOrFail($id);
   
        $request->validate([
            'blog_name' => ['required', 'string', 'max:191'],
            'blog_detail' => ['required', 'string'],
            'blog_thumbnail' => ['nullable', 'mimes:jpg,jpeg,png'],
        ]);

        $blog->blog_name = $request->blog_name;
        $blog->blog_detail = $request->blog_detail;

        if($request->hasFile('blog_thumbnail')){
            Storage::delete($this->file_location.'/'.$blog->blog_thumbnail);

            $uploadedFile = $request->file('blog_thumbnail');        
            $filename = 'blog-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/gambar_blog/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $blog->blog_thumbnail = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }

        $blog->save();
        
        return redirect()->route('dashboard.blog.index')->with([
            'status' => 'success',
            'message' => 'Blog successfully updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        Blog::destroy($blog->id);

        return redirect()->route('dashboard.blog.index')->with([
            'status' => 'success',
            'message' => 'Blog successfully deleted'
        ]);
    }

    public function datatableIndex()
    {
        $blog = Blog::query()->get()->map(function($data, $key){
            $data->blog_name = ucwords($data->blog_name);

            return $data;
        });

        return datatables()
            ->of($blog)
            ->toJson();
    }
}
