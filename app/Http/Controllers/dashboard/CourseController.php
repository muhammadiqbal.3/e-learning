<?php

namespace App\Http\Controllers\dashboard;

use Storage;
use Carbon\Carbon;
use App\Course;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class CourseController extends Controller
{
    protected $file_location = 'public/course';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['course'] = Course::all();
        return view('content.dashboard.course.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.dashboard.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'course_name' => ['required', 'string', 'max:191'],
            'course_detail' => ['required', 'string'],
            'course_thumbnail' => ['nullable', 'mimes:jpg,jpeg,png'],
        ]);

        $course = new Course;
        $course->course_name = $request->course_name;
        $course->course_detail = $request->course_detail;

        if($request->hasFile('course_thumbnail')){
            $uploadedFile = $request->file('course_thumbnail');        
            $filename = 'course-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/gambar_course/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $course->course_thumbnail = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }

        $course->save();

        return redirect()->route('dashboard.course.index')->with([
            'status' => 'success',
            'message' => 'Course successfully added'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('content.dashboard.course.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        // $course = Course::findOrFail($id);
        
        $request->validate([
            'course_name' => ['required', 'string', 'max:191'],
            'course_detail' => ['required', 'string'],
            'course_thumbnail' => ['nullable', 'mimes:jpg,jpeg,png'],
        ]);

        $course->course_name = $request->course_name;
        $course->course_detail = $request->course_detail;
        
        if($request->hasFile('course_thumbnail')){
            Storage::delete($this->file_location.'/'.$course->course_thumbnail);

            $uploadedFile = $request->file('course_thumbnail');        
            $filename = 'course-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/gambar_course/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $course->course_thumbnail = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }

        $course->save();
        
        return redirect()->route('dashboard.course.index')->with([
            'status' => 'success',
            'message' => 'Course successfully updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        Course::destroy($course->id);
        return redirect()->route('dashboard.course.index')->with([
            'status' => 'success',
            'message' => 'Course successfully deleted'
        ]);
    }

    public function datatableIndex()
    {
        $course = Course::query()->get()->map(function($data, $key){
            $data->course_name = ucwords($data->course_name);

            return $data;
        });

        return datatables()
            ->of($course)
            ->toJson();
    }
}
