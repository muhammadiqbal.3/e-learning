<?php

namespace App\Http\Controllers\dashboard;

use Storage;
use Carbon\Carbon;
use App\Exercise;
use App\Question;
use App\Answer;
use App\Lesson;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = DB::table('t_lesson')
                ->get();
        
        return view('content.dashboard.practice.index')->with('question', $question);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.dashboard.practice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     // 'course_name' => ['required', 'string', 'max:191'],
        //     // 'lesson_name' => ['required', 'string', 'max:191'],
        //     // 'question' => ['required', 'string', 'max:191'],
        //     // 'score' => ['required', 'integer'],
        //     // 'answer1' => ['required', 'string'],
        //     // 'answer2' => ['required', 'string'],
        //     // 'answer3' => ['required', 'string'],
        //     // 'answer4' => ['required', 'string'],
        //     // 'answer5' => ['required', 'string'],
        //     // 'correct' => ['required'],
        //     // 'review' => ['required', 'string'],
        //     // 'published' => ['required', 'enum'],
        // ]);

        // $question = new Question;
        // $question->lesson_id = $request->lesson_name;
        // $question->question = $request->question;
        // $question->score = $request->score;
        // // $exercise->answer = $request->answer;
        // // $exercise->review = $request->review;
        // // $exercise->published = $request->published;
        
        // $question->save();
        
        // $answers = [$request->answer1, $request->answer2, $request->answer3, $request->answer4, $request->answer5];
        
        // for($i=0;$i<5;$i++){
        //     $answer = new Answer;

        //     $answer->question_id = $question->id;
        //     $answer->answer = $answers[$i];
        //     $answer->correct = $request->correct;

        //     $answer->save();
        // }
        $answer = $request->input('answer');
        $correct = $request->input('correct');
        $question = $request->input('question');
        $jawaban = array();
        $quest = 0;
        $index = 0;
        foreach($question as $q){
            $data_question = array(
                'lesson_id' => $request->input('lesson_name'),
                'question' => $q,
                'score' => 20
            );
            Question::insert($data_question);
            $question_new = Question::orderBy('id', 'DESC')->first();
            
            for ($i=1; $i <= 5; $i++) { 
                if(isset($correct[$quest+$i])){
                    $correct_flag = 'true';
                }else{
                    $correct_flag = 'false';
                }
                array_push($jawaban, array(
                    'question_id' => $question_new['id'],
                    'answer' => $answer[$quest+$i],
                    'correct' => $correct_flag
                ));
            }
            $index++;
            $i -= 1;
            $quest = $index*$i;
        }
        Answer::insert($jawaban);
        $lesson = Lesson::findOrFail($request->input('lesson_name'));
        if($request->input('published')){
            $publish_flag = "published";
        }else{
            $publish_flag = "pending";
        }
        $lesson->status = $publish_flag;
        $lesson->save();
        return redirect()->route('dashboard.practice.index')->with([
            'status' => 'success',
            'message' => 'Practice successfully added'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = DB::table('t_lesson')->where('lesson_name',$id)->first();
        $question = DB::table('t_question')
                ->join('t_answer', 't_answer.question_id', '=', 't_question.id')
                ->select('t_question.*', 't_answer.answer','t_answer.correct')
                ->where('lesson_id',$status->id)->get();
        return view('content.dashboard.practice.show')->with(['question' => $question,'status' => $status]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lesson  $exercise
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ls = DB::table('t_lesson')->where('id',$id)->first();
        $question = DB::table('t_question')
                ->where('lesson_id',$ls->id)->get();
        return view('content.dashboard.practice.edit', compact('ls', 'question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exercise $exercise)
    {
        $answer = $request->input('answer');
        $correct = $request->input('correct');
        $question = $request->input('question');
        $id_question = $request->input('id_question');
        $id_answer = $request->input('id_answer');
        
        // $jawaban = array();

        $quest = 0;
        $index = 0;
        foreach($question as $q){
            $qu = Question::findOrFail($id_question[$index+1]);
            $qu->lesson_id = $request->input('lesson_name');
            $qu->question = $q;
            $qu->score = 20;
            $qu->save();
            
            for ($i=1; $i <= 5; $i++) { 
                if(isset($correct[$quest+$i])){
                    $correct_flag = 'true';
                }else{
                    $correct_flag = 'false';
                }
                $ans = Answer::findOrFail($id_answer[$quest+$i]);
                $ans->question_id = $id_question[$index+1];
                $ans->answer = $answer[$quest+$i];
                $ans->correct = $correct_flag;
                $ans->save();
            }
            $index++;
            $i -= 1;
            $quest = $index*$i;
        }
        $lesson = Lesson::findOrFail($request->input('lesson_name'));
        if($request->input('published')){
            $publish_flag = "published";
        }else{
            $publish_flag = "pending";
        }
        $lesson->status = $publish_flag;
        $lesson->save();
        // $exercise = Exercise::findOrFail($id);
        
        // $request->validate([
        //     // 'course_name' => ['required', 'string', 'max:191'],
        //     'lesson_name' => ['required', 'string', 'max:191'],
        //     'question' => ['required', 'string', 'max:191'],
        //     'score' => ['required', 'integer'],
        //     'answer1' => ['required', 'string'],
        //     'answer2' => ['required', 'string'],
        //     'answer3' => ['required', 'string'],
        //     'answer4' => ['required', 'string'],
        //     'answer5' => ['required', 'string'],
        //     'correct' => ['required'],
        //     // 'review' => ['required', 'string'],
        //     // 'published' => ['required', 'enum'],
        // ]);

        // $question->lesson_id = $request->lesson_name;
        // $question->question = $request->question;
        // $question->score = $request->score;
        // // $exercise->answer = $request->answer;
        // // $exercise->review = $request->review;
        // // $exercise->published = $request->published;
        
        // $question->save();
        
        // $answers = [$request->answer1, $request->answer2, $request->answer3, $request->answer4, $request->answer5];
        
        // for($i=0;$i<5;$i++){

        //     $answer->question_id = $question->id;
        //     $answer->answer = $answers[$i];
        //     $answer->correct = $request->correct;

        //     $answer->save();
        // }
        
        return redirect()->route('dashboard.practice.index')->with([
            'status' => 'success',
            'message' => 'Practice successfully updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $data =  DB::table('t_question')->where('lesson_id',$id)->get();
        foreach($data as $a){
            DB::table('t_answer')->where('question_id',$a->id)->delete();
        }
        DB::table('t_question')->where('lesson_id',$id)->delete();

        $lesson = Lesson::findOrFail($id);
        $publish_flag = "pending";
        if($lesson->status == "published"){
            $publish_flag = "pending";
        }
        $lesson->status = $publish_flag;
        $lesson->save();

        return redirect()->route('dashboard.practice.index')->with([
            'status' => 'success',
            'message' => 'Practice successfully deleted'
        ]);
    }

    public function datatableIndex()
    {
        $exercise = DB::table('t_exercise')
                ->join('t_question', 't_question.id', '=', 't_exercise.question_id')
                ->join('t_answer', 't_answer.id', '=', 't_exercise.answer_id')
                ->select('t_exercise.*', 't_lesson.*', 't_question.*', 't_answer.*')
                ->get();
        return datatables()
            ->of($exercise)
            ->toJson();

        return view('dashboard.practice.index');
    }
}
