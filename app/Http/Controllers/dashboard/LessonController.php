<?php

namespace App\Http\Controllers\dashboard;

use Storage;
use Carbon\Carbon;
use App\Course;
use App\Lesson;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class LessonController extends Controller
{
    protected $file_location = 'public/lesson';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $lesson = DB::table('t_lesson')->get();
        $lesson = DB::table('t_lesson')
                ->join('t_course', 't_course.id', '=', 't_lesson.course_id')
                ->select('t_lesson.*', 't_course.course_name')
                ->get();
        // dd($lesson);
        return view('content.dashboard.lesson.index', compact('lesson'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('content.dashboard.lesson.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'course_name' => ['required', 'string', 'max:191'],
            'lesson_name' => ['required', 'string', 'max:191'],
            'lesson_detail' => ['required', 'string'],
            'lesson_thumbnail' => ['nullable', 'mimes:jpg,jpeg,png'],
            'lesson_video' => ['nullable', 'string'],
        ]);

        $lesson = new Lesson;
        $lesson->course_id = $request->course_name;
        $lesson->lesson_name = $request->lesson_name;
        $lesson->lesson_detail = $request->lesson_detail;

        if($request->hasFile('lesson_thumbnail')){
            $uploadedFile = $request->file('lesson_thumbnail');        
            $filename = 'lesson-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/gambar_lesson/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $lesson->lesson_thumbnail = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }

        // if($request->hasFile('lesson_video')){
        //     $uploadedFile = $request->file('lesson_video');        
        //     $filename = 'lesson-'.(Carbon::now()->timestamp+rand(1,1000));
        //     $uploadedFile->move('assets/video_lesson/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
        //     // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
        //     $lesson->lesson_video = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        // }
        // if($request->hasFile('lesson_thumbnail')){
        //     $video_tmp = Input::file('lesson_thumbnail');
        //     $video_name = $video_tmp->getClientOriginalName();
        //     $video_path = 'assets/videos';
        //     $video_tmp->move($video_path,$video_name);
        //     $lesson->lesson_thumbnail = $video_name;
        // }
        $lesson->lesson_video = $request->lesson_video;
        $lesson->save();

        return redirect()->route('dashboard.lesson.index')->with([
            'status' => 'success',
            'message' => 'Lesson successfully added'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $lesson = Lesson::findOrFail($id);
        $lesson = DB::table('t_lesson')->join('t_course', 't_course.id', '=', 't_lesson.course_id')
        ->select('t_lesson.*', 't_course.*')->whereLessonName($id)->first();
        return view('content.dashboard.lesson.show', compact('lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        // $lesson = Lesson::findOrFail($id);
        // dd($lesson);
        return view('content.dashboard.lesson.edit', compact('lesson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson = Lesson::findOrFail($id);

        $request->validate([
            'course_name' => ['required', 'string', 'max:191'],
            'lesson_name' => ['required', 'string', 'max:191'],
            'lesson_detail' => ['required', 'string'],
            'lesson_thumbnail' => ['nullable', 'mimes:jpg,jpeg,png'],
            'lesson_video' => ['nullable', 'string'],
        ]);
        
        $lesson->course_id = $request->course_name;
        $lesson->lesson_name = $request->lesson_name;
        $lesson->lesson_detail = $request->lesson_detail;

        if($request->hasFile('lesson_thumbnail')){
            Storage::delete($this->file_location.'/'.$lesson->lesson_thumbnail);

            $uploadedFile = $request->file('lesson_thumbnail');        
            $filename = 'lesson-'.(Carbon::now()->timestamp+rand(1,1000));
            $uploadedFile->move('assets/gambar_lesson/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
            // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
            $lesson->lesson_thumbnail = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        }

        // if($request->hasFile('lesson_video')){
        //     Storage::delete($this->file_location.'/'.$lesson->lesson_thumbnail);

        //     $uploadedFile = $request->file('lesson_video');        
        //     $filename = 'lesson-'.(Carbon::now()->timestamp+rand(1,1000));
        //     $uploadedFile->move('assets/video_lesson/', $filename.'.'.$uploadedFile->getClientOriginalExtension());
        //     // $path = $uploadedFile->storeAs($this->file_location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
        //     $lesson->lesson_video = $filename.'.'.$uploadedFile->getClientOriginalExtension();
        // }
        $lesson->lesson_video = $request->lesson_video;
        $lesson->save();
        
        return redirect()->route('dashboard.lesson.index')->with([
            'status' => 'success',
            'message' => 'Lesson successfully updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        Lesson::destroy($lesson->id);
        return redirect()->route('dashboard.lesson.index')->with([
            'status' => 'success',
            'message' => 'Lesson successfully deleted'
        ]);
    }

    public function datatableIndex()
    {
        $lesson = DB::table('t_lesson')
                ->join('t_course', 't_course.id', '=', 't_lesson.course_id')
                ->select('t_lesson.*', 't_course.course_name', 't_course.course_thumbnail', 't_course.course_detail')
                ->get();

        return datatables()
            ->of($lesson)
            ->toJson();

        return view('dashboard.lesson.index');
    }
}
