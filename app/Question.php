<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 't_question';

    protected $fillable = [
        'question',
        'score'
    ];

    public function lesson()
    {
        return $this->belongsTo('App\Lesson', 'lesson_id');
    }

    public function exercise()
    {
        return $this->hasMany('App\Exercise', 'question_id');
    }

    public function answer()
    {
        return $this->hasMany('App\Answer', 'question_id');
    }
}
