<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_lesson', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->string('lesson_name');
            $table->string('lesson_thumbnail');
            $table->string('lesson_video');
            $table->longText('lesson_detail');
            $table->enum('status', ['published', 'pending'])->default('pending');
            $table->timestamps();

            $table->foreign('course_id')
                  ->references('id')
                  ->on('t_course')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_lesson');
    }
}
