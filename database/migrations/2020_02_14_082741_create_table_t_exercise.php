<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTExercise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_exercise', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('question_id')->unsigned();
            $table->bigInteger('answer_id')->unsigned();
            $table->longText('review');
            $table->enum('status', ['published', 'pending'])->default('pending');
            $table->timestamps();

            $table->foreign('question_id')
                  ->references('id')
                  ->on('t_question')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('answer_id')
                  ->references('id')
                  ->on('t_answer')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_exercise');
    }
}
