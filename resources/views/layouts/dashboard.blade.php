@if(auth()->user()->role == 'admin')
    <!DOCTYPE html>
    <html lang="en">
    <!-- BEGIN HEAD -->

    <head>
        <title>Dashboard | Erporate E-Learning</title>
        <!-- google font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <link href="{{ asset('dashboard_asset/fonts/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard_asset/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard_asset/fonts/material-design-icons/material-icon.css') }}" rel="stylesheet" type="text/css" />
        <!--bootstrap -->
        <link href="{{ asset('dashboard_asset/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard_asset/assets/plugins/summernote/summernote.css') }}" rel="stylesheet">
        <!-- inbox style -->
        <link href="{{ asset('dashboard_asset/assets/css/pages/inbox.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Material Design Lite CSS -->
        <link rel="stylesheet" href="{{ asset('dashboard_asset/assets/plugins/material/material.min.css') }}">
        <link rel="stylesheet" href="{{ asset('dashboard_asset/assets/css/material_style.css') }}">
        <!-- dropzone -->
        <link href="{{ asset('dashboard_asset/assets/plugins/dropzone/dropzone.css') }}" rel="stylesheet" media="screen">
        <!-- Date Time item CSS -->
        <link rel="stylesheet" href="{{ asset('dashboard_asset/assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css') }}" />
        <!-- Theme Styles -->
        <link href="{{ asset('dashboard_asset/assets/css/theme/light/theme_style.css') }}" rel="stylesheet" id="rt_style_components" type="text/css" />
        <link href="{{ asset('dashboard_asset/assets/css/theme/light/style.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard_asset/assets/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard_asset/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard_asset/assets/css/theme/light/theme-color.css') }}" rel="stylesheet" type="text/css" />
        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('dashboard_asset/assets/img/favicon/favicon.ico') }}" type="image/x-icon" />
        @yield('plugins_css')

        @yield('inline_css')
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-dark">
        
        @yield('content')

        <div class="page-wrapper">

        @include('layouts.partials.dashboard.navbar')
            <!-- start page container -->
            <div class="page-container">
                @include('layouts.partials.dashboard.sidebar')

                @yield('content_alert')
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                    @yield('content_header')

                    @yield('content_body')
                    </div>
                </div>

                @include('layouts.partials.dashboard.footer')
            </div>
            <!-- end page container -->
        </div>
        @yield('plugins_js')
        @yield('inline_js')
        {{-- Javascript --}}
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        <!-- start js include path -->
        <script src="{{ asset('dashboard_asset/assets/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/popper/popper.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/jquery-blockui/jquery.blockui.min.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
        <!-- bootstrap -->
        <script src="{{ asset('dashboard_asset/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/sparkline/jquery.sparkline.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/pages/sparkline/sparkline-data.js') }}"></script>
        <!-- Common js-->
        <script src="{{ asset('dashboard_asset/assets/js/app.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/layout.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/theme-color.js') }}"></script>
        <!-- material -->
        <script src="{{ asset('dashboard_asset/assets/plugins/material/material.min.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/pages/material-select/getmdl-select.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/material-datetimepicker/moment-with-locales.min.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/material-datetimepicker/datetimepicker.js') }}"></script>
        <!-- dropzone -->
        <script src="{{ asset('dashboard_asset/assets/plugins/dropzone/dropzone.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/dropzone/dropzone-call.js') }}"></script>
        <!-- chart js -->
        <script src="{{ asset('dashboard_asset/assets/plugins/chart-js/Chart.bundle.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/chart-js/utils.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/pages/chart/chartjs/home-data2.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/plugins/sparkline/jquery.sparkline.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/pages/sparkline/sparkline-data.js') }}"></script>
        <!-- summernote -->
        <script src="{{ asset('dashboard_asset/assets/plugins/summernote/summernote.js') }}"></script>
        <script src="{{ asset('dashboard_asset/assets/js/pages/summernote/summernote-data.js') }}"></script>
        <!-- end js include path -->
    </body>

    </html>
@else
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">

        <!-- Title-->
        <title>Erporate | E-learning</title>

        <!-- viewport scale-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="{{ asset('public_asset/assets/img/favicon/favicon.gif') }}">
        <link rel="shortcut icon" href="{{ asset('public_asset/assets/img/favicon/114x114.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('public_asset/assets/img/favicon/96x96.png') }}">
        <!--Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">
        <!-- Icon fonts -->
        <link rel="stylesheet" href="{{ asset('public_asset/assets/fonts/fontawesome/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('public_asset/assets/fonts/themify-icons/css/themify-icons.css') }}">
        <!-- stylesheet-->
        <link rel="stylesheet" href="{{ asset('public_asset/assets/css/vendors.bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('public_asset/assets/css/style.css') }}">

    </head>

    <body>

        @include('layouts.partials.public.navbar')

        @yield('content')

        @include('layouts.partials.public.footer')
        @include('layouts.partials.public.copyright')

        <div class="scroll-top ">
            <i class="ti-angle-up "></i>
        </div>

        <script src="{{ asset('public_asset/assets/js/vendors.bundle.js') }}"></script>
        <script src="{{ asset('public_asset/assets/js/scripts.js') }}"></script>
    </body>

    </html>
@endif