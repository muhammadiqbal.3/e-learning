<!-- start sidebar menu -->
<div class="sidebar-container">
                <div class="sidemenu-container navbar-collapse collapse fixed-menu">
                    <div id="remove-scroll" class="left-sidemenu">
                        <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard') }}" class="nav-link nav-toggle">
                                    <i class="material-icons">dashboard</i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link nav-toggle"> <i class="material-icons">school</i>
                                    <span class="title">Class</span> <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{ route('dashboard.course.index') }}" class="nav-link ">
                                            <span class="title">
												Course
											</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('dashboard.lesson.index') }}" class="nav-link ">
                                            <span class="title">
												Lesson
											</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.practice.index') }}" class="nav-link nav-toggle"> <i class="material-icons">local_library</i>
                                    <span class="title">Practice</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.blog.index') }}" class="nav-link nav-toggle"> <i class="material-icons">description</i>
                                    <span class="title">Blog</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.student.index') }}" class="nav-link nav-toggle"> <i class="material-icons">group</i>
                                    <span class="title">Students</span>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="#" class="nav-link nav-toggle"> <i class="material-icons">description</i>
                                    <span class="title">Pages</span> <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="#gmap_static" class="nav-link ">
                                            <span class="title">
												FAQ
											</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link ">
                                            <span class="title">
												Contact Us
											</span>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->
                        </ul>
                    </div>
                </div>
</div>
<!-- end sidebar menu -->