<div class="footer-bottom bg-black-0_9 py-5 text-center ">
        <div class="container ">
            <p class="text-white-0_5 mb-0 ">Copyright <a href="http://erporate.com " target="_blank ">PT. Erporate Solusi Global</a> - E-Learning &copy;
                <script>
                    document.write(new Date().getFullYear());
                </script>
                All rights reserved
            </p>
        </div>
    </div>
    <!-- END copyright -->