<nav class="ec-nav sticky-top bg-white">
        <div class="container">
            <div class="navbar p-0 navbar-expand-lg">
                <div class="navbar-brand">
                    <a class="logo-default" href="{{ route('public.index') }}"><img alt="" src="{{ asset('public_asset/assets/img/logos/logo-erporate.png') }}"></a>
                </div>
                <span aria-expanded="false" class="navbar-toggler ml-auto collapsed" data-target="#ec-nav__collapsible" data-toggle="collapse">
                  <div class="hamburger hamburger--spin js-hamburger">
                  <div class="hamburger-box">
                      <div class="hamburger-inner"></div>
                  </div>
                  </div>
              </span>
                <div class="collapse navbar-collapse when-collapsed" id="ec-nav__collapsible">
                    <ul class="nav navbar-nav ec-nav__navbar ml-auto">

                        <li class="nav-item nav-item__has-megamenu megamenu-col-2">
                            <a class="nav-link" href="{{ route('public.index') }}">Beranda</a>

                        </li>
                        
                        <li class="nav-item nav-item__has-dropdown">
                            @guest
                                @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="nav-link"> {{ __('Kelas') }} </a>
                                @endif
                            @else
                                <a class="nav-link" href="{{ route('course') }}">Kelas</a>
                            @endguest
                        </li>

                        <!-- <li class="nav-item nav-item__has-dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Information</a>
                            <div class="dropdown-menu">
                                <ul class="list-unstyled">
                                    <li>
                                        <a class="nav-link__list" href="#"> FAQs </a>
                                    </li>
                                    <li>
                                        <a class="nav-link__list" href="#"> About Us </a>
                                    </li>
                                </ul>
                            </div>
                        </li> -->

                        @guest
                            <li class="nav-item nav-item__has-dropdown">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Masuk') }}</a>
                            </li>
                            @if (Route::has('register'))    
                                <li class="nav-item nav-item__has-megamenu">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Daftar') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item nav-item__has-dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ _('Profile') }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <ul class="list-unstyled">
                                        <li>
                                            @if(auth()->user()->role != 'student')
                                                <a class="dropdown-item" href="{{ route('dashboard') }}">
                                                    {{ Auth::user()->name }}
                                                </a>
                                            @else
                                                <a class="dropdown-item" href="{{ route('profile') }}">
                                                    {{ Auth::user()->name }}
                                                </a>
                                            @endif
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                {{ __('Keluar') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li> -->
                        @endguest
                    </ul>
                    <!-- </div>
              <div class="nav-toolbar">
                  <ul class="navbar-nav ec-nav__navbar">
                      <li class="nav-item">
                          <a class="nav-link site-search-toggler" href="#">
                              <i class="ti-search"></i>
                          </a>
                      </li>
                  </ul>
              </div> -->
                </div>
            </div>
            <!-- END container-->
        </div>
    </nav>
    <!-- END nav -->

    <!-- <div class="site-search">
      <div class="site-search__close bg-black-0_8"></div>
      <form class="form-site-search" action="#" method="POST">
          <div class="input-group">
              <input type="text" placeholder="Search" class="form-control py-3 border-white" required="">
              <div class="input-group-append">
                  <button class="btn btn-primary" type="submit"><i class="ti-search"></i></button>
              </div>
          </div>
      </form>
  </div> -->
    <!-- END site-search-->