<footer class="site-footer ">
        <div class="footer-top bg-dark text-white-0_6 pt-5 paddingBottom-70 ">
            <div class="container ">
                <div class="row ">

                    <div class="col-lg-4 col-md-6 mt-5 ">
                        <img src="{{ asset('public_asset/assets/img/logos/logo2.png') }}" alt="Logo " style="width: 200px; ">
                        <div class="margin-y-40 ">
                            <p>
                                Erporate Indonesia adalah sebuah digital agency yang antusias membangun produk digital dengan cara efektif dan kreatif.
                            </p>
                        </div>
                        <ul class="list-inline ">
                            <li class="list-inline-item "><a class="iconbox bg-white-0_2 hover:primary " href="https://www.facebook.com/erporate "><i class="ti-facebook "> </i></a></li>
                            <li class="list-inline-item "><a class="iconbox bg-white-0_2 hover:primary " href="https://twitter.com/erporate "><i class="ti-twitter "> </i></a></li>
                            <li class="list-inline-item "><a class="iconbox bg-white-0_2 hover:primary " href="https://www.linkedin.com/company/pterporatesolusiglobal "><i class="ti-linkedin "> </i></a></li>
                            <li class="list-inline-item "><a class="iconbox bg-white-0_2 hover:primary " href="https://plus.google.com/100771217264928077260 "><i class="ti-google "></i></a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6 mt-5 ">
                        <h3 class="text-white ">Alamat</h3>
                        <div class="width-3rem bg-primary height-3 mt-3 "></div>
                        <div class="marginTop-40 ">
                            <p class="mb-4 ">
                                <h5 class="text-white ">Yogyakarta</h5>
                                <div class="media ">
                                    <i class="ti-location-pin mt-2 mr-3 "></i>
                                    <div class="media-body ">
                                        <span>Jl. Wirajaya No. 310A RT 01/02 Kelurahan Condong Catur, Depok, Sleman.</span>
                                    </div>
                                </div>
                            </p>
                            <!-- <p class="mb-4 ">
                                <h5 class="text-white ">Jakarta</h5>
                                <div class="media ">
                                    <i class="ti-location-pin mt-2 mr-3 "></i>
                                    <div class="media-body ">
                                        <span>Jakarta Barat, Madison Park, Magnolia Tower 10BJ, Tanjung Duren Selatan.</span>
                                    </div>
                                </div>
                            </p>                             -->
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 mt-5 ">
                        <h3 class="text-white ">Kontak Kami</h3>
                        <div class="width-3rem bg-primary height-3 mt-3 "></div>
                        <ul class="list-unstyled marginTop-40 ">
                            <li class="mb-4 "><i class="ti-mobile mr-3 "></i><a href=" ">(0274) 2834390 </a></li>
                            <li class="mb-4 "><i class="ti-mobile mr-3 "></i><a href="https://api.whatsapp.com/send?phone=6281393733771 ">081393733771 </a>(HRD)</li>
                            <li class="mb-4 "><i class="ti-mobile mr-3 "></i><a href="https://api.whatsapp.com/send?phone=6285742486880 ">085742486880 </a>(HRD2)</li>
                            <li class="mb-4 "><i class="ti-email mr-3 "></i><a href="mailto:erporate@gmail.com ">erporate@gmail.com</a></li>
                        </ul>
                    </div>

                </div>
                <!-- END row-->
            </div>
            <!-- END container-->
        </div>
        <!-- END footer-top-->
    </footer>
    <!-- END site-footer -->