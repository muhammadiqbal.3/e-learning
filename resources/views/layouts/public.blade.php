<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <!-- Title-->
    <title>Erporate | E-learning</title>

    <!-- viewport scale-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('public_asset/assets/img/favicon/favicon.gif') }}">
    <link rel="shortcut icon" href="{{ asset('public_asset/assets/img/favicon/114x114.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('public_asset/assets/img/favicon/96x96.png') }}">
    <!--Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">
    <!-- Icon fonts -->
    <link rel="stylesheet" href="{{ asset('public_asset/assets/fonts/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('public_asset/assets/fonts/themify-icons/css/themify-icons.css') }}">
    <!-- stylesheet-->
    <link rel="stylesheet" href="{{ asset('public_asset/assets/css/vendors.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('public_asset/assets/css/style.css') }}">

</head>

<body>

    @include('layouts.partials.public.navbar')

    @yield('content')

    @include('layouts.partials.public.footer')
    @include('layouts.partials.public.copyright')

    <div class="scroll-top ">
        <i class="ti-angle-up "></i>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="{{ asset('public_asset/assets/js/vendors.bundle.js') }}"></script>
    <script src="{{ asset('public_asset/assets/js/scripts.js') }}"></script>
    @yield('inline_js')
</body>

</html>