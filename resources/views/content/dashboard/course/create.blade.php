@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Add Course</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="{{ route('dashboard.course.index') }}">All Courses</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Add Course</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Course Details</header>
            </div>
            <form method="POST" action="{{ route('dashboard.course.store') }}" enctype="multipart/form-data">
                <div class="card-body row">
                    {{ csrf_field() }}
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input type="text" name="course_name" id="course_name" class="mdl-textfield__input @error('course_name') is-invalid @enderror" value="{{ old('course_name') }}">
                            <label class="mdl-textfield__label">Course Name</label>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20">
                        
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            Course Details
                            <label class="mdl-textfield__label" for="course_detail">Course Details</label>
                            <textarea class="mdl-textfield__input @error('course_detail') is-invalid @enderror" name="course_detail" id="summernote" cols="30" rows="10">
                                {{ old('course_detail') }}
					        </textarea>     
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20">
                        <label class="control-label col-md-3">
                            Course Thumbnail <br>
                            (.jpg, .png, .jpeg)
                        </label>
                        <input type="file" accept=".jpg,.jpeg,.png" name="course_thumbnail" id="course_thumbnail" class="form-control @error('course_thumbnail') is-invalid @enderror">
                    </div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
                        <a href="{{ route('dashboard.course.index') }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>                    
                    </div>    
                </div>
            </form>
        </div>
    </div>
</div>
@endsection