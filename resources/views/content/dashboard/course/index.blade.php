@extends('layouts.dashboard')

@section('plugins_css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<!-- data tables -->
<link href="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/lightcase/css/lightcase.css') }}">
@endsection

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">All Courses</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">All Courses</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
@if(session('status') == 'success')
    <div class="alert alert-success">{{ session('message') }}</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-line">
            <div class="tab-content">
                <div class="tab-pane active fontawesome-demo" id="tab1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-aqua">
                                <div class="card-head">
                                    <header>Course List</header>
                                </div>
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <div class="btn-group">
                                                <a href="{{ route('dashboard.course.create') }}" id="addRow" class="btn btn-info">
													Add New <i class="fa fa-plus"></i>
												</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="courseTable">
                                            <thead>
                                                <tr>
                                                    <th class="center"> No. </th>
                                                    <th class="center"> Thumbnail Course </th>
                                                    <th class="center"> Course Name </th>
                                                    <th class="center"> Detail Course </th>
                                                    <th class="center"> Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($course as $c)
                                                <tr class="odd gradeX">
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td class="patient-img center">
                                                        <a href="{{ url('assets/gambar_course/'.$c->course_thumbnail) }}" class="" data-rel="lightcase">
                                                            <img src="{{ asset('assets/gambar_course/'.$c->course_thumbnail) }}" alt="{{ $c->course_name }}">
                                                        </a>
                                                    </td>
                                                    <td>{{ $c->course_name }}</td>
                                                    <td class="left">{{ $c->course_detail }}</td>
                                                    <td class="center">
                                                        <a href="{{ route('dashboard.course.index').'/'.$c->id.'/edit' }}" class="btn btn-warning btn-xs">
                                                            <i class="fa fa-pencil"></i>Edit
                                                        </a>
                                                        <form action="{{ route('dashboard.course.index').'/'.$c->id }}" method="post" class="d-inline">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit" class="btn btn-danger btn-xs">
                                                                <i class="fa fa-trash-o "></i>Delete
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins_js')
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/datatable/datatables.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- datatables -->
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/js/pages/table/table_data.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/lightcase/js/lightcase.js') }}"></script>
@endsection

@section('inline_js')
<script>
    var courseTable = $("#courseTable").DataTable({
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        columnDefs: [
            {
                targets: 4,
                orderable: false,
                searchable: false,
            }
        ],
        responsive: true
    });

    $(document).ready(function(){
        $('a[data-rel^=lightcase]').lightcase();
        $('#slideshowTable').on('draw.dt', function() {
            $('a[data-rel^=lightcase]').lightcase();
        });
    });
    $('body').on('click', 'a[data-rel^=lightcase]', function(e) {
        var href = $(this).attr('href');
        lightcase.start({
            href: href
        });
        e.preventDefault();
    });
</script>
<!-- <script>
    var courseTable = $("#courseTable").DataTable({
        serverSide: true,
        processing: true,
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        ajax: "{{ route('datatable.course.index') }}",
        columns: [
            {data: 'id'},
            {data: 'course_thumbnail'},
            {data: 'course_name'},
            {data: 'course_detail'},
            {data: ''},
        ],
        columnDefs: [
            {
                targets: 4,
                orderable: false,
                searchable: false,
                render: function(row, type, data){
                    // console.log(row)
                    let url = "{{ route('dashboard.course.index') }}";
                    return "<div class='btn-group'>"
                        +"<a href='"+url+"/edit/"+data.id+"' class='btn btn-warning btn-xs'><i class='far fa-pencil'></i>Edit</a>"
                        +"&nbsp;&nbsp;&nbsp;"
                        +"<a href='"+url+"/"+data.id+"' class='btn btn-danger btn-xs'><i class='far fa-trash'></i>Delete</a>"
                    +"</div>"
                }
            }
        ],
        responsive: true
    });
</script> -->
@endsection