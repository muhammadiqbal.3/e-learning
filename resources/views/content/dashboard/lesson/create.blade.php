@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Add Lesson</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="{{ route('dashboard.lesson.index') }}">All Lessons</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Add Lesson</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Lesson Details</header>
            </div>
            <form method="POST" action="{{ route('dashboard.lesson.store') }}" enctype="multipart/form-data">
                <div class="card-body row">
                {{ csrf_field() }}
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                            <label class="" for="course_name">Course Name</label><br/>
                            <select name="course_name">
                                <?php
                                    $course = DB::table('t_course')->get();
                                    
                                    foreach($course as $data){
                                ?>
                                
                                    <option value="{{ $data->id }}">{{ $data->course_name }}</option>
                                
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <label class="">Lesson Name</label>
                            <input type="text" name="lesson_name" id="lesson_name" class="mdl-textfield__input @error('lesson_name') is-invalid @enderror" value="{{ old('lesson_name') }}">
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            
                            <label class="" for="lesson_detail">Lesson Details</label>
                            <textarea class="mdl-textfield__input @error('lesson_detail') is-invalid @enderror" name="lesson_detail" id="summernote" cols="30" rows="10">
                                {{ old('lesson_detail') }}
					        </textarea>
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <label class="control-label col-md-6">
                            Lesson Thumbnail <br>
                            (.jpg, .jpeg, .png)
                        </label>
                        <input type="file" accept=".jpg,.jpeg,.png" name="lesson_thumbnail" id="lesson_thumbnail" class="form-control @error('lesson_thumbnail') is-invalid @enderror">
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <label class="control-label col-md-6">
                            Lesson Video <br>
                            (paste an embed youtube url)
                        </label>
                        <input type="text" name="lesson_video" id="lesson_video" class="mdl-textfield__input @error('lesson_video') is-invalid @enderror" value="{{ old('lesson_video') }}">
                        <!-- <input type="file" accept=".mp4,.mov,.avi" name="lesson_video" id="lesson_video" class="form-control @error('lesson_video') is-invalid @enderror"> -->
                    </div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
                        <a href="{{ route('dashboard.lesson.index') }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection