@extends('layouts.dashboard')

@section('plugins_css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<!-- data tables -->
<link href="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/lightcase/css/lightcase.css') }}">
@endsection

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">All Blogs</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">All Blogs</li>
        </ol>
    </div>
</div>
@endsection

@section('content_alert')
<div id="alert_section">
    @if(Session::get('message'))
        <div class="alert alert-result alert-{{ Session::get('status') ? Session::get('status') : 'secondary' }} alert-dismissible fade show mb-0" role="alert">
            <span>{{ Session::get('message') }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
</div>
@endsection

@section('content_body')
@if(session('status') == 'success')
    <div class="alert alert-success">{{ session('message') }}</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-line">
            <div class="tab-content">
                <div class="tab-pane active fontawesome-demo" id="tab1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-aqua">
                                <div class="card-head">
                                    <header>Blog List</header>
                                </div>
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <div class="btn-group">
                                                <a href="{{ route('dashboard.blog.create') }}" id="addRow" class="btn btn-info">
													Add New <i class="fa fa-plus"></i>
												</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="blogTable">
                                            <thead>
                                                <tr>
                                                    <th class="center"> No. </th>
                                                    <th class="center"> Thumbnail Blog </th>
                                                    <th class="center"> Blog Name </th>
                                                    <!-- <th class="center"> Detail Blog </th> -->
                                                    <th class="center"> Action </th>
                                                </tr>
                                            </thead>
                                            @foreach($blog as $b)
                                                <tr class="odd gradeX">
                                                    <td class="center">{{ $loop->iteration }}</td>
                                                    <td class="patient-img center">
                                                        <a href="{{ url('assets/gambar_blog/'.$b->blog_thumbnail) }}" class="" data-rel="lightcase">
                                                            <img src="{{ asset('assets/gambar_blog/'.$b->blog_thumbnail) }}" alt="{{ $b->blog_name }}">
                                                        </a>                                                       
                                                    </td>
                                                    <td>{{ $b->blog_name }}</td>
                                                    <!-- <td class="left">{!! $b->blog_detail !!}</td> -->
                                                    <td class="center">
                                                        <a href="{{ route('dashboard.blog.show', $b->blog_name) }}" class="btn btn-primary btn-xs">
                                                            <i class="fa fa-eye"></i>Details
                                                        </a>
                                                        <a href="{{ route('dashboard.blog.index').'/'.$b->id.'/edit' }}" class="btn btn-warning btn-xs">
                                                            <i class="fa fa-pencil"></i>Edit
                                                        </a>
                                                        <form action="{{ route('dashboard.blog.index').'/'.$b->id }}" method="post" class="d-inline">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit" class="btn btn-danger btn-xs">
                                                                <i class="fa fa-trash-o "></i>Delete
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card card-box">
            <div class="card-head">
                <header>Blog List</header>
            </div>
            <div class="card-body no-padding height-9">
                <div class="row table-padding">
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="btn-group">
                            <a href="{{ route('dashboard.blog.create') }}" id="addRow" class="btn btn-info">
								Add New <i class="fa fa-plus"></i>
							</a>
                        </div>
                    </div>
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <div class="btn-group pull-right">
                                                <a class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
													<i class="fa fa-angle-down"></i>
												</a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-print"></i> Print </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                </div>
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="blogTable">
                        <thead>
                            <tr>
                                <th class="center">No.</th>
                                <th class="center">Thumbnail Blog</th>
                                <th class="center">Blog Name</th>
                                <th class="center">Blog Detail</th>
                                <th class="center">Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td class="center">1</td>
                                <td class="patient-img center">
                                    <img src="../assets/img/std/std1.jpg" alt="">
                                </td>
                                <td class="center"> Expand Your Programming Knowledge </td>
                                <td class="center"> Investig ationes demons trave runt lectores legere liusry quod </td>
                                <td class="center">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-primary dropdown-toggle center no-margin" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
											<i class="fa fa-angle-down"></i>
										</button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a href="edit_blog.html">
                                                    <i class="fa fa-pencil"></i>
													Edit 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-trash-o"></i>
													Delete
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection

@section('plugins_js')
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/datatable/datatables.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- datatables -->
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/js/pages/table/table_data.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/lightcase/js/lightcase.js') }}"></script>
@endsection

@section('inline_js')
<script>
    var blogTable = $("#blogTable").DataTable({
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        columnDefs: [
            {
                targets: 3,
                orderable: false,
                searchable: false,
            }
        ],
        responsive: true
    });

    $(document).ready(function(){
        $('a[data-rel^=lightcase]').lightcase();
        $('#slideshowTable').on('draw.dt', function() {
            $('a[data-rel^=lightcase]').lightcase();
        });
    });
    $('body').on('click', 'a[data-rel^=lightcase]', function(e) {
        var href = $(this).attr('href');
        lightcase.start({
            href: href
        });
        e.preventDefault();
    });
</script>
<!-- <script>
    var blogTable = $("#blogTable").DataTable({
        serverSide: true,
        processing: true,
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        ajax: "{{ route('datatable.blog.index') }}",
        columns: [
            {data: 'id'},
            {data: 'blog_thumbnail'},
            {data: 'blog_name'},
            {data: 'blog_detail'},
            {data: ''},
        ],
        columnDefs: [
            {
                targets: 4,
                orderable: false,
                searchable: false,
                render: function(row, type, data){
                    // console.log(row);
                    let url = "{{ route('dashboard.blog.index') }}";
                    
                    return "<div class='btn-group'>"
                        +"<a href='"+url+"/edit/"+data.id+"' class='btn btn-warning btn-xs'><i class='far fa-pencil'></i>Edit</a>"
                        +"&nbsp;&nbsp;&nbsp;"
                        +"<a href='"+url+"/destroy/"+data.id+"' class='btn btn-danger btn-xs'><i class='far fa-trash'></i>Delete</a>"
                    +"</div>"
                }
            }
        ],
        responsive: true
    });
</script> -->
@endsection