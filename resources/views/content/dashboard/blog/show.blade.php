@extends('layouts.dashboard')

@section('plugins_css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<!-- data tables -->
<link href="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/lightcase/css/lightcase.css') }}">
@endsection

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Blog Details</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="{{ route('dashboard.blog.index') }}">All Blogs</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Blog Details</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Blog Details</header>
            </div>
            
                <table class="table table-striped">
                
                    <tr>
                        <th>Blog Name</th>
                        <td>{{ $blog->blog_name }}</td>
                    </tr>
                    <tr>
                        <th>Blog Thumbnail</th>
                        <td class="patient-img">
                            <a href="{{ url('assets/gambar_blog/'.$blog->blog_thumbnail) }}" class="" data-rel="lightcase"><img src="{{ asset('assets/gambar_blog/'.$blog->blog_thumbnail) }}" alt="{{ $blog->blog_name }}"></a>                            
                        </td>
                    </tr>
                    <tr>
                        <th>Blog Detail</th>
                        <td>{!! $blog->blog_detail !!}</td>
                    </tr>
                
                </table>
                <div class="center">
                    <a href="{{ route('dashboard.blog.index').'/'.$blog->id.'/edit' }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-warning">Edit</a>
                    <a href="{{ route('dashboard.blog.index') }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Back</a>
                </div>
            
        </div>
    </div>
</div>
@endsection

@section('plugins_js')
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/datatable/datatables.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- datatables -->
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/js/pages/table/table_data.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/lightcase/js/lightcase.js') }}"></script>
@endsection

@section('inline_js')
<script>
    $(document).ready(function(){
        $('a[data-rel^=lightcase]').lightcase();
        $('#slideshowTable').on('draw.dt', function() {
            $('a[data-rel^=lightcase]').lightcase();
        });
    });
    $('body').on('click', 'a[data-rel^=lightcase]', function(e) {
        var href = $(this).attr('href');
        lightcase.start({
            href: href
        });
        e.preventDefault();
    });
</script>
@endsection