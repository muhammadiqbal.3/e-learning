@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Edit Blog</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="{{ route('dashboard.blog.index') }}">All Blogs</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Edit Blog</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Blog Details</header>
            </div>
            <form method="POST" action="{{ route('dashboard.blog.update', $blog->id) }}" enctype="multipart/form-data">
                <div class="card-body row">
                    {{ csrf_field() }}
                    @method('PUT')
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input class="mdl-textfield__input @error('blog_name') is-invalid @enderror" name="blog_name" id="blog_name" type="text" value="{{ $blog->blog_name }}" id="txtBlogName">
                            <label class="mdl-textfield__label">Blog Name</label>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            Blog Details
                            <label class="mdl-textfield__label" for="blog_detail">Blog Details</label>
                            <textarea class="mdl-textfield__input @error('blog_detail') is-invalid @enderror" name="blog_detail" id="summernote" cols="30" rows="10">
                                {!! $blog->blog_detail !!}
                            </textarea>
                            <input type="hidden" id="old_blog" value="{{ $blog->blog_detail }}" readonly>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20">
                        <label class="control-label col-md-3">
                            Blog Thumbnail<br>
                            (.jpg, .png, .jpeg)
                        </label>
                        <input type="file" accept=".jpg,.jpeg,.png" name="blog_thumbnail" id="blog_thumbnail" class="form-control @error('blog_thumbnail') is-invalid @enderror">
                        <!-- <div class="col-md-12">
                            <div id="id_dropzone" class="dropzone"></div>
                        </div> -->
                    </div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
                        <a href="{{ route('dashboard.blog.index') }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection