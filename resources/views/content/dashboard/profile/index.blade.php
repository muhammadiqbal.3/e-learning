@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Profile</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Profile</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <div class="card card-topline-aqua">
                <div class="card-body no-padding height-9">
                    <div class="row">
                        <div class="profile-userpic">
                            @if(Auth::user()->photo)
                                <img src="{{ asset('assets/profile/'.$user->photo) }}" class="img-responsive" alt=""> 
                            @else
                                <img src="{{ asset('assets/blank/blank-profile.png') }}" class="img-responsive" alt=""> 
                            @endif
                        </div>
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">{{ Auth::user()->name }}</div>
                        <div class="profile-usertitle-job">{{ Auth::user()->role }}</div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <!-- <div class="profile-userbuttons">
                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                    </div>
                        END SIDEBAR BUTTONS -->
                </div>
            </div>
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-head card-topline-aqua">
                            <header>Account Detail</header>
                        </div>
                        <div class="card-body no-padding height-9">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form method="POST" action="{{ route('profile.update', Auth::user()) }}" enctype="multipart/form-data">
                                            <div class="panel">
                                                <div class="card-body row">
                                                {{ csrf_field() }} 
												@method('PUT')
                                                    <div class="col-lg-6 p-t-20">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                            <label class="">Name</label>
                                                            <input class="mdl-textfield__input @error('name') is-invalid @enderror" name="name" id="name" type="text" value="{{ Auth::user()->name }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 p-t-20">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                            <label class="">Username</label>    
                                                            <input class="mdl-textfield__input @error('username') is-invalid @enderror" name="username" id="username" type="text" value="{{ Auth::user()->username }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 p-t-20">
                                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                            <label class="">Email</label>    
                                                            <input class="mdl-textfield__input @error('email') is-invalid @enderror" name="email" id="email" type="text" value="{{ Auth::user()->email }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="panel">
                                                <div class="card-body row">
                                                    <div class="col-lg-12 p-t-20 text-center">
                                                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
                                                        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-warning" data-toggle="modal" data-target="#formChangePass">Change Password</button>
                                                        <a href="#" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-danger">Cancel</a>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="formChangePass" tabindex="-1" role="dialog" aria-labelledby="formChangePass" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="car modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="changePassForm">
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group" id="field_old_password">
                        <label for="old_password" class="col-form-label">Old Password</label>
                        <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Old Password">    
                    </div>
                    <div class="form-group" id="field_password">
                        <label for="password" class="col-form-label">New Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="col-form-label">Re-Type New Password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Re-Type Password">    
                    </div>

                    <div class="form-group text-right mb-0">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('inline_js')
<script>
    $("#changePassForm").submit(function(e){
        console.log("Change Password is running");
        e.preventDefault();
        $(".alert-result").slideUp(function(){
            $(this).remove();
        });

        $("#changePassForm input").removeClass('is-invalid');
        $("#changePassForm .invalid-feedback").remove();

        $.post("{{ route('profile.password', Auth::user()) }}", $(this).serialize() ,function(result){
            console.log(result);
            $("#formChangePass").modal('hide');

            $("#old_password").val('');
            $("#password").val('');
            $("#password_confirmation").val('');

            // Append Alert Result
            $('<div class="alert alert-result alert-'+result.status+' alert-dismissible fade show mb-0" role="alert"><span>'+result.message+'</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>').appendTo($("#alert_section")).slideDown("slow", "swing");
        }).fail(function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            $.each(jqXHR.responseJSON.errors, function(key, result) {
                $("#"+key).addClass('is-invalid');
                $("#field_"+key).append('<div class="invalid-feedback">'+result+'</div>');
            });
        });
    });
</script>
@endsection