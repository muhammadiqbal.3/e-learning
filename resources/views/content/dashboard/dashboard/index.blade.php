@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
        <div class="page-title">Dashboard</div>
    </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li class="active">
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<!-- start widget -->
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="row clearfix">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="card card-topline-aqua">
                    <div class="panel-body">
                        <h3>Total Course</h3>
                        <div class="text-center">
                            <h1>{{ ($course)->count() }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="card card-topline-aqua">
                    <div class="panel-body">
                        <h3>Total lesson</h3>
                        <div class="text-center">
                            <h1>{{ ($lesson)->count() }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="card card-topline-aqua">
                    <div class="panel-body">
                        <h3>Total Blog</h3>
                        <div class="text-center">
                            <h1>{{ ($blog)->count() }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="card card-topline-aqua">
                    <div class="panel-body">
                        <h3>Total Students</h3>
                        <div class="text-center">
                            <h1>{{ ($user)->count() }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end widget -->
@endsection