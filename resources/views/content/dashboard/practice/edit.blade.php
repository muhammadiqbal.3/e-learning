@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Edit Practice</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="index.html">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="all_practice.html">All Practices</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Edit Practice</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Practice Details</header>
            </div>
            <div class="card-body row">
                <form method="POST" action="{{ route('dashboard.practice.update', $ls->id) }}" class="card-body row">
                {{ csrf_field() }}
                    @method('PUT')
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                            <!-- <input class="mdl-textfield__input" type="text" id="lesson_name" value="" readonly tabIndex="-1">
                            <label for="lesson_name" class="pull-right margin-0">
                                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                            </label> -->
                            <label class="mdl-textfield__label" for="lesson_name">Lesson Name</label><br/>
                            <select name="lesson_name">
                            
                                <?php
                                    $lesson = DB::table('t_lesson')->get();
                                    
                                    foreach($lesson as $data){
                                ?>
                                    @if($data->id == $ls->id)
                                        <option value="{{ $data->id }}" selected>{{ $data->lesson_name }}</option>
                                    @else
                                        <option value="{{ $data->id }}">{{ $data->lesson_name }}</option>
                                    @endif
                                    
                                
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <?php 
                        $quest = 0;
                        $index = 0;
                    ?>
                    @foreach($question as $q_data)
                        <div class="col-lg-12 p-t-20">
                            <input type="hidden" name="id_question[{{ $loop->iteration }}]" value="{{ $q_data->id }}">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="h5" for="question">Question {{ $loop->iteration }}</label>
                                <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question[{{ $loop->iteration }}]" id="" cols="" rows="">
                                    {{ $q_data->question }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <label class="">Score</label>
                                <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12 p-t-20 center">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <div class="h4">Check For The Correct Answer!</div>
                            </div>
                        </div>
                        <?php
                            $answer = DB::table('t_answer')->where('question_id',$q_data->id)->get();
                            
                            $i = 1;
                            foreach($answer as $aw){ 
                        ?>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <input type="hidden" name="id_answer[{{ $quest+$i }}]" value="{{ $aw->id }}">
                                <label class="" for="answer1">Answer {{ $i }}</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer" name="answer[{{ $quest+$i }}]">{{ $aw->answer }}</textarea>
                                @if($aw->correct == "true")
                                    <input type="checkbox" checked name="correct[{{ $quest+$i }}]" id="answer_correct1">
                                @else
                                    <input type="checkbox" name="correct[{{ $quest+$i }}]" id="answer_correct1">
                                @endif
                                
                            </div>
                        </div>
                            <?php 
                            $i++;    
                        } 
                            $index++;
                            $i -= 1;
                            $quest = $index*$i;
                        ?>
                    @endforeach
<!-- 
                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="question">Question 2</label>
                            <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question" id="" cols="" rows="">
                                {{ old('question') }}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-lg-2 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <label class="">Score</label>
                            <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20 center">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            Check For The Correct Answer!
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer1">Answer 1</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer1"></textarea>
                            <input type="checkbox" name="correct1" id="answer_correct1">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer2">Answer 2</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer2"></textarea>
                            <input type="checkbox" name="correct2" id="answer_correct2">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer3">Answer 3</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer3"></textarea>
                            <input type="checkbox" name="correct3" id="answer_correct3">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer4">Answer 4</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer4"></textarea>
                            <input type="checkbox" name="correct4" id="answer_correct4">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer5">Answer 5</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer5"></textarea>
                            <input type="checkbox" name="correct5" id="answer_correct5">
                        </div>
                    </div>

                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="question">Question 3</label>
                            <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question" id="" cols="" rows="">
                                {{ old('question') }}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-lg-2 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <label class="">Score</label>
                            <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20 center">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            Check For The Correct Answer!
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer1">Answer 1</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer1"></textarea>
                            <input type="checkbox" name="correct1" id="answer_correct1">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer2">Answer 2</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer2"></textarea>
                            <input type="checkbox" name="correct2" id="answer_correct2">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer3">Answer 3</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer3"></textarea>
                            <input type="checkbox" name="correct3" id="answer_correct3">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer4">Answer 4</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer4"></textarea>
                            <input type="checkbox" name="correct4" id="answer_correct4">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer5">Answer 5</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer5"></textarea>
                            <input type="checkbox" name="correct5" id="answer_correct5">
                        </div>
                    </div>

                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="question">Question 4</label>
                            <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question" id="" cols="" rows="">
                                {{ old('question') }}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-lg-2 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <label class="">Score</label>
                            <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20 center">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            Check For The Correct Answer!
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer1">Answer 1</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer1"></textarea>
                            <input type="checkbox" name="correct1" id="answer_correct1">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer2">Answer 2</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer2"></textarea>
                            <input type="checkbox" name="correct2" id="answer_correct2">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer3">Answer 3</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer3"></textarea>
                            <input type="checkbox" name="correct3" id="answer_correct3">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer4">Answer 4</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer4"></textarea>
                            <input type="checkbox" name="correct4" id="answer_correct4">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer5">Answer 5</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer5"></textarea>
                            <input type="checkbox" name="correct5" id="answer_correct5">
                        </div>
                    </div>

                    <div class="col-lg-12 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="question">Question 5</label>
                            <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question" id="" cols="" rows="">
                                {{ old('question') }}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-lg-2 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <label class="">Score</label>
                            <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20 center">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            Check For The Correct Answer!
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer1">Answer 1</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer1"></textarea>
                            <input type="checkbox" name="correct1" id="answer_correct1">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer2">Answer 2</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer2"></textarea>
                            <input type="checkbox" name="correct2" id="answer_correct2">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer3">Answer 3</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer3"></textarea>
                            <input type="checkbox" name="correct3" id="answer_correct3">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer4">Answer 4</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer4"></textarea>
                            <input type="checkbox" name="correct4" id="answer_correct4">
                        </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                            <label class="" for="answer5">Answer 5</label>
                            <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer5"></textarea>
                            <input type="checkbox" name="correct5" id="answer_correct5">
                        </div>
                    </div> -->

                    <div class="col-lg-12 p-t-20">
                    @if($ls->status == "published")
                        <label class="control-label col-md-3" id="published_label" for="published">
                            Published
                        </label>
                        <div class="col-md-12">
                            <input type="checkbox" checked name="published" id="published" value="show" {{ old('published') == 'show' ? 'checked' : '' }} onchange="checkStatus()">
                        </div>
                    @else
                    <label class="control-label col-md-3" id="published_label" for="published">
                            Pending
                        </label>
                        <div class="col-md-12">
                            <input type="checkbox" name="published" id="published" value="show" {{ old('published') == 'show' ? 'checked' : '' }} onchange="checkStatus()">
                        </div>
                    @endif
                    </div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
                        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('inline_js')
<script>
    // if(document.getElementById('answer_correct1').checked) {
    //     document.getElementById('answer_correct1').value = 'true';
    // }else if(document.getElementById('answer_correct2').checked) {
    //     document.getElementById('answer_correct2').value = 'true';
    // }else if(document.getElementById('answer_correct3').checked) {
    //     document.getElementById('answer_correct3').value = 'true';
    // }else if(document.getElementById('answer_correct4').checked) {
    //     document.getElementById('answer_correct4').value = 'true';
    // }else if(document.getElementById('answer_correct5').checked) {
    //     document.getElementById('answer_correct5').value = 'true';
    // }

    $(document).ready(function(){
        checkStatus();
    });

    function checkStatus(){
        if($("#published").prop('checked') === true){
            $("#published_label").text('Published');
        } else {
            $("#published_label").text('Pending');
        }
    }
</script>
@endsection