@extends('layouts.dashboard')

@section('plugins_css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<!-- data tables -->
<link href="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/datatable/datatables.css') }}">
@endsection

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">All Practices</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">All Practices</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
@if(session('status') == 'success')
    <div class="alert alert-success">{{ session('message') }}</div>
@endif
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Practice List</header>
            </div>
            <div class="card-body no-padding height-9">
                <div class="row table-padding">
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="btn-group">
                            <a href="{{ route('dashboard.practice.create') }}" id="addRow" class="btn btn-info">
								Add New <i class="fa fa-plus"></i>
							</a>
                        </div>
                    </div>
                </div>
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="practiceTable">
                        <thead>
                            <tr>
                                <th class="center">No.</th>
                                <th class="center">Lesson Name</th>
                                <th class="center">Status</th>
                                <th class="center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($question as $q_data)
                            <?php
                                    $q = DB::table('t_question')->where('lesson_id', $q_data->id)->first();
                                    if(!isset($q)){
                                        continue;
                                    }
                                ?>
                                <tr class="odd gradeX">
                                    <td class="center"> {{ $loop->iteration }} </td>
                                    <td class="center"> {{ $q_data->lesson_name }} </td>
                                    <td class="center"> {{ $q_data->status }} </td>
                                    <td class="center">
                                        <a href="{{ route('dashboard.practice.show', $q_data->lesson_name) }}" class="btn btn-primary btn-xs">
                                            <i class="fa fa-eye"></i>Detail
                                        </a>
                                        <a href="{{ route('dashboard.practice.index').'/'.$q_data->id.'/edit' }}" class="btn btn-warning btn-xs">
                                            <i class="fa fa-pencil"></i>Edit
                                        </a>
                                        <form action="{{ route('dashboard.practice.index').'/'.$q_data->id }}" method="post" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash-o "></i>Delete
                                            </button>
                                        </form>
                                        <!-- <div class="btn-group">
                                            <button class="btn btn-xs btn-primary dropdown-toggle center no-margin" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-left" role="menu">
                                                <li>
                                                    <a href="{{ route('dashboard.practice.index').'/'.$q_data->id.'/edit' }}">
                                                        <i class="fa fa-pencil"></i>
                                                        Edit 
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('dashboard.practice.index').'/'.$q_data->id.'/show' }}">
                                                        <i class="fa fa-file"></i>
                                                        Detail
                                                    </a>
                                                </li>
                                                <form action="{{ route('dashboard.practice.index').'/'.$q_data->id }}" method="post" class="">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash-o "></i>Delete
                                                    </button>
                                                </form>
                                            </ul>
                                        </div> -->
                                    </td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins_js')
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/datatable/datatables.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- datatables -->
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/js/pages/table/table_data.js') }}"></script>
@endsection

@section('inline_js')
<script>
    var practiceTable = $("#practiceTable").DataTable({
        order: [0, 'asc'],
        pageLength: 4,
        aLengthMenu:[5,10,15,25,50],
        columnDefs: [
            {
                targets: 3,
                orderable: false,
                searchable: false,
            }
        ],
        responsive: true
    });
</script>
<!-- <script>
    var practiceTable = $("#practiceTable").DataTable({
        serverSide: true,
        processing: true,
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        ajax: "{{ route('datatable.practice.index') }}",
        columns: [
            {data: 'id'},
            {data: 'lesson_name'},
            {data: 'question'},
            {data: 'published'},
            {data: ''},
        ],
        columnDefs: [
            {
                targets: 4,
                orderable: false,
                searchable: false,
                render: function(row, type, data){
                    // console.log(row);
                    let url = "{{ route('dashboard.practice.index') }}";
                    
                    return "<div class='btn-group'>"
                        +"<a href='"+url+"/show/"+data.id+"' class='btn btn-info btn-xs'><i class='far fa-eye'></i>Detail</a>"
                        +"&nbsp;&nbsp;&nbsp;"
                        +"<a href='"+url+"/edit/"+data.id+"' class='btn btn-warning btn-xs'><i class='far fa-pencil'></i>Edit</a>"
                        +"&nbsp;&nbsp;&nbsp;"
                        +"<a href='"+url+"/destroy/"+data.id+"' class='btn btn-danger btn-xs'><i class='far fa-trash'></i>Delete</a>"
                    +"</div>"
                }
            }
        ],
        responsive: true
    });
</script> -->
@endsection