@extends('layouts.dashboard')

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Add Practice</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="{{ route('dashboard.practice.index') }}">All Practices</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Add Practice</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Practice Details</header>
            </div>
            <div class="card-body row">
                <form method="POST" action="{{ route('dashboard.practice.store') }}" class="card-body row">
                    {{ csrf_field() }}
                    <div class="col-lg-6 p-t-20">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                            <!-- <input class="mdl-textfield__input" type="text" id="lesson_name" value="" readonly tabIndex="-1">
                            <label for="lesson_name" class="pull-right margin-0">
                                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                            </label> -->
                            <label class="" for="lesson_name">Lesson Name</label><br/>
                            <select name="lesson_name">
                                <?php
                                    $lesson = DB::table('t_lesson')->get();
                                    
                                    foreach($lesson as $data){
                                ?>
                                
                                    <option value="{{ $data->id }}">{{ $data->lesson_name }}</option>
                                
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                        <div class="col-lg-12 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="h5" for="question">Question 1</label>
                                <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question[1]" id="" cols="" rows="">
                                    {{ old('question') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <label class="">Score</label>
                                <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12 p-t-20 center">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <div class="h4">Check For The Correct Answer!</div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer1">Answer 1</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer[1]"></textarea>'
                                <input type="checkbox" name="correct[1]" id="answer_correct">
                            </div>
                        </div> 
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer2">Answer 2</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer[2]"></textarea>
                                <input type="checkbox" name="correct[2]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer3">Answer 3</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer[3]"></textarea>
                                <input type="checkbox" name="correct[3]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer4">Answer 4</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer[4]"></textarea>
                                <input type="checkbox" name="correct[4]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer5">Answer 5</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer[5]"></textarea>
                                <input type="checkbox" name="correct[5]" id="answer_correct">
                            </div>
                        </div>
                    
                        <div class="col-lg-12 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="h5" for="question">Question 2</label>
                                <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question[2]" id="" cols="" rows="">
                                    {{ old('question') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <label class="">Score</label>
                                <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12 p-t-20 center">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <div class="h4">Check For The Correct Answer!</div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer1">Answer 1</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer[6]"></textarea>
                                <input type="checkbox" name="correct[6]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer2">Answer 2</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer[7]"></textarea>
                                <input type="checkbox" name="correct[7]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer3">Answer 3</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer[8]"></textarea>
                                <input type="checkbox" name="correct[8]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer4">Answer 4</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer[9]"></textarea>
                                <input type="checkbox" name="correct[9]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer5">Answer 5</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer[10]"></textarea>
                                <input type="checkbox" name="correct[10]" id="answer_correct">
                            </div>
                        </div>
                    
                        <div class="col-lg-12 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="h5" for="question">Question 3</label>
                                <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question[3]" id="" cols="" rows="">
                                    {{ old('question') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <label class="">Score</label>
                                <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12 p-t-20 center">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <div class="h4">Check For The Correct Answer!</div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer1">Answer 1</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer[11]"></textarea>
                                <input type="checkbox" name="correct[11]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer2">Answer 2</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer[12]"></textarea>
                                <input type="checkbox" name="correct[12]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer3">Answer 3</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer[13]"></textarea>
                                <input type="checkbox" name="correct[13]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer4">Answer 4</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer[14]"></textarea>
                                <input type="checkbox" name="correct[14]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer5">Answer 5</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer[15]"></textarea>
                                <input type="checkbox" name="correct[15]" id="answer_correct">
                            </div>
                        </div>
                    
                        <div class="col-lg-12 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="h5" for="question">Question 4</label>
                                <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question[4]" id="" cols="" rows="">
                                    {{ old('question') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <label class="">Score</label>
                                <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12 p-t-20 center">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <div class="h4">Check For The Correct Answer!</div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer1">Answer 1</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer[16]"></textarea>
                                <input type="checkbox" name="correct[16]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer2">Answer 2</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer[17]"></textarea>
                                <input type="checkbox" name="correct[17]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer3">Answer 3</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer[18]"></textarea>
                                <input type="checkbox" name="correct[18]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer4">Answer 4</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer[19]"></textarea>
                                <input type="checkbox" name="correct[19]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer5">Answer 5</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer[20]"></textarea>
                                <input type="checkbox" name="correct[20]" id="answer_correct">
                            </div>
                        </div>
                    
                        <div class="col-lg-12 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="h5" for="question">Question 5</label>
                                <textarea class="mdl-textfield__input @error('question') is-invalid @enderror" name="question[5]" id="" cols="" rows="">
                                    {{ old('question') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-lg-2 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <label class="">Score</label>
                                <input class="mdl-textfield__input" type="text" value="20" id="txtCourseName" name="score" readonly>
                            </div>
                        </div>
                        <div class="col-lg-12 p-t-20 center">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <div class="h4">Check For The Correct Answer!</div>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer1">Answer 1</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer1" name="answer[21]"></textarea>
                                <input type="checkbox" name="correct[21]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer2">Answer 2</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer2" name="answer[22]"></textarea>
                                <input type="checkbox" name="correct[22]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer3">Answer 3</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer3" name="answer[23]"></textarea>
                                <input type="checkbox" name="correct[23]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer4">Answer 4</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer4" name="answer[24]"></textarea>
                                <input type="checkbox" name="correct[24]" id="answer_correct">
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <label class="" for="answer5">Answer 5</label>
                                <textarea class="mdl-textfield__input" rows="4" id="answer5" name="answer[25]"></textarea>
                                <input type="checkbox" name="correct[25]" id="answer_correct">
                            </div>
                        </div>
                    

                    <div class="col-lg-12 p-t-20">
                        <label class="control-label col-md-3" id="published_label" for="published">
                            Published
                        </label>
                        <div class="col-md-12">
                            <input type="checkbox" name="published" id="published" value="show" {{ old('published') == 'show' ? 'checked' : '' }} onchange="checkStatus()">
                        </div>
                    </div>
                    <div class="col-lg-12 p-t-20 text-center">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-primary">Submit</button>
                        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('inline_js')
<script>
    // if(document.getElementById('answer_correct1').checked){
    //     document.getElementById('answer_correct1').value = 'true';
    // }
    // if(document.getElementById('answer_correct1').checked) {
    //     document.getElementById('answer_correct1').value = 'true';
    // }else if(document.getElementById('answer_correct2').checked) {
    //     document.getElementById('answer_correct2').value = 'true';
    // }else if(document.getElementById('answer_correct3').checked) {
    //     document.getElementById('answer_correct3').value = 'true';
    // }else if(document.getElementById('answer_correct4').checked) {
    //     document.getElementById('answer_correct4').value = 'true';
    // }else if(document.getElementById('answer_correct5').checked) {
    //     document.getElementById('answer_correct5').value = 'true';
    // }

    $(document).ready(function(){
        checkStatus();
    });

    function checkStatus(){
        if($("#published").prop('checked') === true){
            $("#published_label").text('Published');
        } else {
            $("#published_label").text('Pending');
        }
    }
</script>
@endsection