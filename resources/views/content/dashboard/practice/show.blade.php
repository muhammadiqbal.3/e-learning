@extends('layouts.dashboard')

@section('plugins_css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<!-- data tables -->
<link href="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/lightcase/css/lightcase.css') }}">
@endsection

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Practice Details</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a class="parent-item" href="{{ route('dashboard.practice.index') }}">All Practices</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Practice Details</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-aqua">
            <div class="card-head">
                <header>Practice Details</header>
            </div>
                <table class="table table-striped">
                    <tr>
                        <th>Lesson Name</th>
                        <td colspan="3">html</td>
                        
                    </tr>
                    <tr>
                        <th>Score (per Question)</th>
                        <td>20</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td colspan="3">{{ $status->status }}</td>
                    </tr>
                </table>
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="practiceTable">
                        <thead>
                            <tr class="text-center">
                                <th>No.</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Correct</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($question as $q_data)
                            <tr class="text-center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $q_data->question }}</td>
                                <td>{{ $q_data->answer }}</td>
                                <td>{{ $q_data->correct }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="center">
                    <a href="{{ route('dashboard.practice.index').'/'.$status->id.'/edit' }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-warning">Edit</a>
                    <a href="{{ route('dashboard.practice.index') }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Back</a>
                </div>
        </div>
    </div>
</div>  
@endsection

@section('plugins_js')
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/datatable/datatables.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- datatables -->
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/js/pages/table/table_data.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/lightcase/js/lightcase.js') }}"></script>
@endsection

@section('inline_js')
<script>
    var practiceTable = $("#practiceTable").DataTable({
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        columnDefs: [
            {
                targets: 0,
                orderable: false,
                searchable: false,
            }
        ],
        responsive: true
    });
</script>
@endsection