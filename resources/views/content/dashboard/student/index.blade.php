@extends('layouts.dashboard')

@section('plugins_css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<!-- data tables -->
<link href="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_asset/plugins/datatable/datatables.css') }}">
@endsection

@section('content_header')
<div class="page-bar">
    <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">All Students</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
                <i class="fa fa-home"></i>&nbsp;
                <a class="parent-item" href="{{ route('dashboard') }}">Home</a>&nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">All Students</li>
        </ol>
    </div>
</div>
@endsection

@section('content_body')
<div class="row">
    <div class="col-md-12">
        <div class="tabbable-line">
            <div class="tab-content">
                <div class="tab-pane active fontawesome-demo" id="tab1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-aqua">
                                <div class="card-head">
                                    <header>Student List</header>
                                </div>
                                <div class="card-body ">
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="studentTable">
                                            <thead>
                                                <tr>
                                                    <th class="center"> No. </th>
                                                    <th class="center"> Name </th>
                                                    <th class="center"> E-mail </th>
                                                    <th class="center"> Gender </th>
                                                    <th class="center"> Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($student as $s)
                                                <tr class="odd gradeX">
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $s->name }}</td>
                                                    <td><a href="mailto:{{ $s->email }}" style="color:black">{{ $s->email }}</a></td>
                                                    <td>{{ $s->gender }}</td>
                                                    <td class="center">
                                                        <a href="{{ route('dashboard.student.show', $s->name) }}" class="btn btn-primary btn-xs">
                                                            <i class="fa fa-eye"></i>Detail
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins_js')
<script type="text/javascript" src="{{ asset('dashboard_asset/plugins/datatable/datatables.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- datatables -->
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dashboard_asset/assets/js/pages/table/table_data.js') }}"></script>
@endsection

@section('inline_js')
<script>
    var studentTable = $("#studentTable").DataTable({
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        columnDefs: [
            {
                targets: 4,
                orderable: false,
                searchable: false,
            }
        ],
        responsive: true
    });
</script>
<!-- <script>
    var courseTable = $("#courseTable").DataTable({
        serverSide: true,
        processing: true,
        order: [0, 'asc'],
        pageLength: 5,
        aLengthMenu:[5,10,15,25,50],
        ajax: "{{ route('datatable.course.index') }}",
        columns: [
            {data: 'id'},
            {data: 'course_thumbnail'},
            {data: 'course_name'},
            {data: 'course_detail'},
            {data: ''},
        ],
        columnDefs: [
            {
                targets: 4,
                orderable: false,
                searchable: false,
                render: function(row, type, data){
                    // console.log(row)
                    let url = "{{ route('dashboard.course.index') }}";
                    return "<div class='btn-group'>"
                        +"<a href='"+url+"/edit/"+data.id+"' class='btn btn-warning btn-xs'><i class='far fa-pencil'></i>Edit</a>"
                        +"&nbsp;&nbsp;&nbsp;"
                        +"<a href='"+url+"/"+data.id+"' class='btn btn-danger btn-xs'><i class='far fa-trash'></i>Delete</a>"
                    +"</div>"
                }
            }
        ],
        responsive: true
    });
</script> -->
@endsection