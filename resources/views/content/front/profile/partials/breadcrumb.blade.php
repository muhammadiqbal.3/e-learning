<div class="py-5 bg-dark">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 text-white">
                <h2>{{ $page_title }}</h2>
            </div>
        </div>
    </div>
</div>