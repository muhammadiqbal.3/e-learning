@extends('layouts.public')

@section('content')
    @include('content.front.profile.partials.breadcrumb', [
        'page_title' => 'Profil'
    ])
<section class="paddingTop-50 paddingBottom-100 bg-light">
        <div class="container">
            <div id="alert_section">
                @if(Session::get('message'))
                    <div class="alert alert-result alert-{{ Session::get('status') ? Session::get('status') : 'secondary' }} alert-dismissible fade show mb-0" role="alert">
                        <span>{{ Session::get('message') }}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        <!-- @if(session('status') == 'success')
            <div class="alert alert-success">{{ session('message') }}</div>
        @endif -->
            <div class="row">
                <div class="col-lg-4 mt-4">
                    <div class="card shadow-v1">
                        <div class="card-header text-center border-bottom pt-5 mb-4">
                            @if(Auth::user()->photo != null)
                                <img class="rounded-circle mb-4" src="{{ url('assets/user_photo/'.Auth::user()->photo) }}" width="200" height="200" alt="">
                            @else
                                    <img class="rounded-circle mb-4" src="{{ asset('assets/blank/blank-profile.png') }}" width="200" height="200" alt="">
                                
                            @endif
                            <h4>
                                {{ Auth::user()->name }}
                            </h4>
                            <p>
                                {{ Auth::user()->username }}
                            </p>
                        </div>
                        <div class="card-body border-bottom">
                            <ul class="list-unstyled">
                                <li class="mb-3">
                                    <span class="d-block">Alamat Email :</span>
                                    <p class="h6">{{ Auth::user()->email }}</p>
                                </li>
                                <li class="mb-3">
                                    <span class="d-block">Jenis Kelamin :</span>
                                    <p class="h6">{{ Auth::user()->gender }}</p>
                                </li>
                                <li class="mb-3">
                                    <span class="d-block">Instansi :</span>
                                    <p class="h6">{{ Auth::user()->institute }}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- END col-md-4 -->
                <div class="col-lg-8 mt-4">
                    <div class="card shadow-v1 padding-30">
                        <ul class="nav tab-line tab-line mb-4" role="tablist">
                            <form method="POST" action="{{ route('profile.update', Auth::user()) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @method('PUT')
                                <div class="tab-pane fade show active" id="Tabs_1-5" role="tabpanel">
                                    <div class="border-bottom mb-4 pb-4">
                                        <h4>
                                            Unggah Foto
                                        </h4>
                                        <div class="media align-items-end mt-4">
                                            <div class="position-relative">
                                                <label class="control-label text-dark">
                                                    Foto Profil
                                                </label>
                                                <input type="file" accept=".jpg,.jpeg,.png" name="photo" id="photo" class="form-control @error('photo') is-invalid @enderror">
                                            </div>
                                        <!-- <div class="position-relative">
                                            <input type="file" class="opacity-0 position-absolute as-parent">
                                            <img src="{{ asset('public_asset/assets/img/placeholder-1.jpg')}}" alt="">
                                        </div> -->
                                        <!-- <div class="media-body ml-4 mb-4 mb-md-0">
                                            <p>
                                                JPG or PNG 200x200 px
                                            </p>
                                            <a href="#"></a>
                                            <button class="btn btn-outline-primary">
                                              <input type="file" class="opacity-0 position-absolute">
                                              Upload
                                            </button>

                                        </div> -->
                                        </div>
                                    </div>

                                    <div class="border-bottom mb-4 pb-4">
                                        <h4 class="mb-4">
                                            Kelola Akun Anda
                                        </h4>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-dark">Nama Lengkap</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ Auth::user()->name }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-5 col-form-label text-dark">Username</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" id="username" value="{{ Auth::user()->username }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-dark">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ Auth::user()->email }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-dark" for="gender">Jenis Kelamin</label>
                                            <div class="col-md-9">
                                                <select name="gender" id="gender" class="form-control">
                                                    <option value="default">-Pilih Jenis Kelamin-</option>
                                                    <option value="Laki-Laki" class="@error('gender') is-invalid @enderror" @if(Auth::user()->gender == "Laki-Laki") selected @endif>Laki-Laki</option>
                                                    <option value="Perempuan" class="@error('gender') is-invalid @enderror" @if(Auth::user()->gender == "Perempuan") selected @endif>Perempuan</option>
                                                </select>
                                                <!-- <input type="tel" class="form-control" value=""> -->
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-dark">Instansi</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control @error('institute') is-invalid @enderror" name="institute" id="institute" value="{{ Auth::user()->institute }}">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="my-5">
                                        <button type="submit" class="btn btn-primary m-2">Perbarui Profil</button>
                                        <button type="button" class="btn btn-warning m-2" data-toggle="modal" data-target="#formChangePass">Ubah Password</button>
                                        <button type="reset" class="btn btn-danger m-2">Batal</button>
                                    </div>
                                </div>
                            <!-- END tab-pane -->
                            </form>
                            <div class="modal fade" id="formChangePass" tabindex="-1" role="dialog" aria-labelledby="formChangePass" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Pengaturan Keamanan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body py-4">
                                            <form id="formChangePass1">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="PUT">
                                                <!-- <input type="hidden" name="_method" value="PUT"> -->
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-dark" for="old_password">Password Lama</label>
                                                    <div class="col-md-9" id="field_old_password">
                                                        <input type="password" class="form-control" placeholder="Password Lama" name="old_password" id="old_password">
                                                        <!-- @error('old_password')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                        @if(session('status') == 'failed1')
                                                            <div class="invalid-feedback">{{ session('message') }}</div>
                                                        @endif -->
                                                    </div>
                                                </div>

                                                <div class="form-group row" >
                                                    <label class="col-md-3 col-form-label text-dark" for="password">Password Baru</label>
                                                    <div class="col-md-9" id="field_password">
                                                        <input type="password" class="form-control" placeholder="Password Baru" name="password" id="password">
                                                        <!-- @error('password')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror
                                                        @if(session('status') == 'failed2')
                                                            <div class="invalid-feedback">{{ session('message') }}</div>
                                                        @endif -->
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-dark" for="password_confirmation">Konfirmasi Password</label>
                                                    <div class="col-md-9">
                                                        <input type="password" class="form-control" placeholder="Konfirmasi Password" name="password_confirmation" id="password_confirmation">
                                                        <!-- @error('password_confirmation')
                                                            <div class="invalid-feedback">{{ $message }}</div>
                                                        @enderror -->
                                                    </div>
                                                </div>
                                                
                                            
                                                <div class="modal-footer py-4">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                    <button type="reset" class="btn btn-danger">Hapus</button>
                                                    <button type="submit" class="btn btn-success">Simpan</button>
                                                    <!-- <button type="button" class="btn btn-success" id="tes">Coba</button> -->
                                                </div>
                                            </form> 
                                        </div>                                       
                                    </div>
                                </div>
                            </div>                      
                        </ul>
                    </div>
                        <!-- END tab-content-->
                </div>
                    <!-- END card-->
            </div>
                <!-- END col-md-8 -->
        </div>
            <!--END row-->
    </div>
        <!--END container-->
</section>
@endsection

@section('inline_js')
<script>
    $("#formChangePass1").submit(function(e){
        console.log("Change Password is running");
        e.preventDefault();
        $(".alert-result").slideUp(function(){
            $(this).remove();
        });

        $("#changePassForm input").removeClass('is-invalid');
        $("#changePassForm .invalid-feedback").remove();

        $.post("{{ route('profile.password', Auth::user()) }}", $(this).serialize() ,function(result){
            console.log(result);
            $("#formChangePass").modal('hide');

            $("#old_password").val('');
            $("#password").val('');
            $("#password_confirmation").val('');

            // Append Alert Result
            $('<div class="alert alert-result alert-'+result.status+' alert-dismissible fade show mb-0" role="alert"><span>'+result.message+'</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>').appendTo($("#alert_section")).slideDown("slow", "swing");
        }).fail(function(jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            $.each(jqXHR.responseJSON.errors, function(key, result) {
                console.log(key);
                $("#"+key).addClass('is-invalid');
                $("#field_"+key).append('<div class="invalid-feedback">'+result+'</div>');
            });
        });
    });
</script>
@endsection