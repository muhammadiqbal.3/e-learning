@extends('layouts.public')

@section('content')

@include('content.front.index.partials.slider')
@include('content.front.index.partials.course')
@include('content.front.index.partials.about')
@include('content.front.index.partials.blog')

@endsection