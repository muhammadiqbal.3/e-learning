<section>
        <div class="container-fluid ">
            <div class="row ">
                <div class="col-md-6 bg-cover bg-center text-white padding-y-80 " style="background:url('{{ asset('public_asset/assets/img/960x560/1.jpg') }}') no-repeat ">
                    <div class="padding-x-lg-100 wow pulse ">
                        <h2 class="text-white mb-4 text-center ">
                            Tonton Video Tutorial
                        </h2>
                        <p>
                            Belajar melalui video tutorial, kapan dan dimana saja menjadi lebih mudah untuk mempelajari programming.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 bg-cover bg-center text-white padding-y-80 " style="background:url('{{ asset('public_asset/assets/img/960x560/2.jpg') }}') no-repeat ">
                    <div class="padding-x-lg-100 wow pulse ">
                        <h2 class="text-white mb-4 text-center ">
                            Soal Latihan
                        </h2>
                        <p>
                            Dengan adanya soal latihan, maka kamu akan lebih memahami materi yang diberikan dari video tutorial.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END container-->
</section>