<section class="height-90vh py-5 flex-center jarallax" data-dark-overlay="4" style="background:url('{{ asset('public_asset/assets/img/1920x800/1.jpg') }}') no-repeat">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 text-white">
                <h2 class="display-lg-4 font-weight-bold text-primary wow slideInUp">
                    Belajar Menjadi Programmer Handal
                </h2>
                <p class="lead wow slideInUp">
                    Mau membuat website dan aplikasi?<br>Bingung mulai dari mana?<br>Selamat bergabung di Erporate E-Learning!<br>Terdapat banyak tutorial gratis untuk menjadi programmer handal dan dapat meningkatkan skill-mu.
                </p>
                @guest
                    @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="btn btn-primary btn-lg mt-3 wow slideInUp"> {{ __('Daftar Gratis') }} </a>
                    @endif
                @else
                    
                @endguest
            </div>
        </div>
    </div>
</section>