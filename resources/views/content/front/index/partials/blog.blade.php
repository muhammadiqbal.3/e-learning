<section class="padding-y-100 bg-light-v5">
    <div class="container">
        <div class="row">

            <div class="col-12 text-center mb-4">
                <h2 class="mb-4">
                    Blog
                </h2>
                <div class="width-3rem height-4 rounded bg-primary mx-auto"></div>
            </div>
        </div>
            <!-- END row-->
            
        <div class="row">
            @if($blog->isEmpty())
                <div class="alert alert-warning text-center" style="width:100%" role="alert">
                    Maaf, Kami belum memiliki blog terbaru!
                </div>
            @else
                @foreach($blog as $item)
                <div class="col-lg-4 col-md-6 marginTop-30">
                    <div href="page-course-details.html" class="card height-100p text-gray shadow-v1">
                        <img class="card-img-top" src="{{ url('assets/gambar_blog/'.$item->blog_thumbnail) }}" alt="">
                        <div class="card-body">
                            <a href="{{ route('blog.show', $item->blog_name) }}" class="h5">{{ $item->blog_name }}</a>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
            <!-- END row-->

        <div class="row mb-5">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" style="top: 50px;">
                <center>
                    <a href="{{ route('blog') }}" class="btn btn-primary btn-lg mt-3 wow fadeIn">SEMUA BLOG</a>
                </center>
            </div>
        </div>
    </div>
        <!-- END container-->
</section>