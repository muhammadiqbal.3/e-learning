@extends('layouts.public')

@section('content')
    @include('content.front.lesson.partials.breadcrumb')

    <section class="pt-5 paddingBottom-150 bg-light-v2">
        <div class="container">
            <div class="row">
                @foreach($lesson as $item)
                <div class="col-lg-9 mt-4">

                    <div class="card">
                        <div class="card-body">
                            @if($item->lesson_video != null)
                                <div class="ec-video-container">
                                    <!-- <video id="player1" poster="../../www.mediaelementjs.com/images/big_buck_bunny.jpg" preload="none" controls>
                                    <source src="https://commondatastorage.googleapis.com/gtv-videos-bucket/CastVideos/mp4/BigBuckBunny.mp4" type="video/mp4">
                                    </video> -->
                                    <!-- <iframe width="560" height="315" src="{{ $item->lesson_video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
                                    {!! $item->lesson_video !!}
                                </div>
                            @else
                                <div class="alert alert-warning text-center" style="width:100%" role="alert">
                                    Maaf, Kami belum memiliki video pelajaran!
                                </div>
                            @endif

                            <h2 class="my-4">
                                {{ $item->lesson_name }}
                            </h2>

                            <!-- <div class="media align-items-center justify-content-between mb-5">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <i class="ti-user mr-2"></i> Andrew Mead
                                    </div>
                                </div>
                            </div> -->

                            <p>
                                {!! $item->lesson_detail !!}
                            </p>
                            <!-- <p>
                                Business and Finance Sequitur <a href="#" class="text-primary">Education & University.</a> Investiga tiones demonstr aver unt lectores legere me lius quod ii qua legunt saepius. Claritas est etiam pro cessus.
                            </p>
                            <blockquote>
                                Design is the fundamental soul of a man-made creation that ends up expressing itself in successive outer layers of the product or service which each element plays together.
                                <cite>
                                    Steve Jobs
                                </cite>
                            </blockquote>
                            <h4>
                                The Ultimate Guide to Game Development
                            </h4>
                            <p>
                                Investig ationes demons trave runt lectores legere liusry quod was legunt saepius claritas Investig tones. Pharetra dui, nec tincidunt ante mauris eu diam. Phasellus verra nisl vitae cursus aei uismod supen dise saepius claritas investig. Investiga tiones
                                demonstr averun d lectores legere melius.
                            </p>
                            <ul class="list-unstyled list-style-icon list-icon-check my-4">
                                <li>Delivers solutions that help drive research analytics lectores </li>
                                <li>Human capital research analytics bringing an approach </li>
                                <li>Complex problems bringing an approach delivers solutions </li>
                                <li>Works with senior executives to help them analytics bringing </li>
                            </ul>
                            <h4 class="text-primary">
                                Creating User Centered Metrics
                            </h4>
                            <p>
                                Investig ationes demons trave runt lectores legere liusry quod was legunt saepius claritas Investig tones. Pharetra dui, nec tincidunt ante mauris eu diam hasellus verra cursus.
                            </p>
                            <div class="row">
                                <div class="col-md-6 mt-4">
                                    <img class="w-100" src="assets/img/360x300/1.jpg" alt="">
                                </div>
                                <div class="col-md-6 mt-4">
                                    <p>
                                        Investig ationes demons trave runt lectores legere liusry quod was legunt saepius claritas Investig tones haretra dui, nec tincidunt ante mauris eu diam. Phasellus verra nisl vitae cursus aei uismod supen dise saepius claritas legere melius tones haretra.
                                    </p>
                                    <p>
                                        Tonx cray commodo, exercitation you probably haven’t heard of them beard cred. Selfies iPhone Kickstarter.
                                    </p>
                                </div>
                            </div>
                            <h4 class="mt-4">
                                UX Writing. Let User Interface Speak
                            </h4>
                            <p>
                                Investig ationes demons trave runt lectores legere liusry quod was legunt saepius claritas Investig tones. Phasellus verrade monstr averun dlectores legere melius verrade monstr averun dlectores.
                            </p>
                            <img class="w-100 my-4" src="assets/img/600x432/1.jpg" alt="">
                            <p>
                                Investig ationes demons trave runt lectores legere liusry quod was legunt saepius claritas Investig tones. Pharetra dui, nec tincidunt ante mauris eu diam. Phasellus verra nisl vitae cursus aei uismod supen dise saepius claritas investig. Investiga tiones
                                demonstr averun dlectores legere melius.
                            </p>
                            <p>
                                Business and Finance Sequitur mutatin onem consuetudium. Investiga tiones demonstr aver unt lectores legere me lius quod ii qua legunt saepius. Claritas est etiam pro cessus.
                            </p>

                            <div class="row align-items-center mb-5">
                                <div class="col-md-6 mt-5 ">
                                    <a href="#" class="btn btn-outline-light btn-pill btn-sm mr-2">#Education</a>
                                    <a href="#" class="btn btn-outline-light btn-pill btn-sm mr-2">#Design</a>
                                    <a href="#" class="btn btn-outline-light btn-pill btn-sm mr-2">#University</a>
                                </div>
                                <div class="col-md-6  mt-5 text-right">
                                    <div class="animated-share">
                                        <a href="#" class="animated-share__trigger btn btn-outline-primary iconbox iconbox-sm">
                                            <i class="ti-share"></i>
                                        </a>
                                        <div class="animated-share__media">
                                            <a href="#" class="btn btn-outline-facebook iconbox iconbox-sm">
                                                <i class="ti-facebook"></i>
                                            </a>
                                            <a href="#" class="btn btn-outline-twitter iconbox iconbox-sm">
                                                <i class="ti-twitter"></i>
                                            </a>
                                            <a href="#" class="btn btn-outline-google-plus iconbox iconbox-sm">
                                                <i class="ti-google"></i>
                                            </a>
                                            <a href="#" class="btn btn-outline-linkedin iconbox iconbox-sm">
                                                <i class="ti-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                        <!-- END card-body-->
                    </div>
                    <!-- END card-->
                </div>
                <!-- END col-lg-9 -->
                
                <aside class="col-lg-3 mt-4">

                    <!-- <div class="widget">
                        <h2 class="widget-title">
                            List Materi :<br> Web Programming
                        </h2>
                        <ul>
                            <?php
                                $lesson = DB::table('t_lesson')->get();
                                  
                                foreach($lesson as $data){
                            ?>
                            
                                <li><a href="{{ route('lesson', $item->id) }}"><i class="ti-arrow-right"></i> {{ $data->lesson_name }}</a></li>
                            
                            <?php } ?>
                            
                        </ul>
                    </div> -->
                    <!-- END widget-->

                    <div class="widget">
                        <h2 class="widget-title">
                            Soal Latihan {{ $item->lesson_name }}
                        </h2>
                        <ul>
                            <li><a href="{{ route('practice', $item->id) }}" class="btn btn-primary btn-lg mt-3 wow fadeIn">Lanjut ke Soal</a></li>
                        </ul>
                    </div>
                    <!-- END widget-->

                </aside>
                @endforeach
                <!-- END col-lg-3 -->
            </div>
            <!-- END row-->
        </div>
        <!-- END container-->
    </section>
    <!-- END section -->

@endsection