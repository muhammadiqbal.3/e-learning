@extends('layouts.public')

@section('content')

    @include('content.front.course.partials.breadcrumb2', [
        'page_before' => 'Semua Kelas',
        'page_title' => $course->course_name,
        'page_location' => $course->course_name
    ])
    @include('content.front.course.partials.content')

@endsection