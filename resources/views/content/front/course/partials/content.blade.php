<section class="paddingTop-50 paddingBottom-100 bg-light-v2">
        <div class="container">
            @if($lesson->isEmpty())
                <div class="alert alert-warning text-center" style="width:100%" role="alert">
                    Maaf, Kami belum memiliki Materi terbaru!
                </div>
            @else
                @foreach($lesson as $item)
                <div class="list-card align-items-center shadow-v1 marginTop-30">
                    <div class="col-lg-4 px-lg-4 my-4">
                        <img class="w-100" src="{{ url('assets/gambar_lesson/'.$item->lesson_thumbnail) }}" alt="{{ $item->lesson_name }}" >
                    </div>
                    <div class="col-lg-8 paddingRight-30 my-4">
                        <div class="media justify-content-between">
                            <div class="group">
                                <a href="{{ route('lesson', $item->id) }}" class="h4">
                                {{ $item->lesson_name }}
                                </a>
                                <!-- <ul class="list-inline mt-3">
                                    <li class="list-inline-item mr-2">
                                        <i class="ti-user mr-2"></i> Andrew Mead
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                        <p>
                            {!! $item->lesson_detail !!}
                        </p>
                    </div>
                </div>
                @endforeach
            @endif
            <!-- <div class="row">
                <div class="col-12 marginTop-70">
                    <ul class="pagination pagination-primary justify-content-center">
                        <li class="page-item mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">
                                <i class="ti-angle-left small"></i>
                            </a>
                        </li>
                        <li class="page-item mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">1</a>
                        </li>
                        <li class="page-item active disabled mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">2</a>
                        </li>
                        <li class="page-item mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">3</a>
                        </li>
                        <li class="page-item disabled mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">
                                <i class="ti-more-alt"></i>
                            </a>
                        </li>
                        <li class="page-item mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">16</a>
                        </li>
                        <li class="page-item mx-1">
                            <a class="page-link iconbox iconbox-sm rounded-0" href="#">
                                <i class="ti-angle-right small"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div> -->
            <!-- END row-->
        </div>
        <!-- END container-->
</section>