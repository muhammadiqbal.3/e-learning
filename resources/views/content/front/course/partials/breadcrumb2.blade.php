<div class="padding-y-60 bg-cover" data-dark-overlay="6" style="background:url('{{ asset('public_asset/assets/img/breadcrumb-bg.jpg') }}') no-repeat">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 my-2 text-white">
                    <ol class="breadcrumb breadcrumb-double-angle bg-transparent p-0">
                        <li class="breadcrumb-item"><a href="{{ route('public.index') }}">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('course') }}">{{ $page_before }}</a></li>
                        <li class="breadcrumb-item">{{ $page_location }}</li>
                    </ol>
                    <h2 class="h1">
                        {{ $page_title }}
                    </h2>
                </div>
            </div>
        </div>
</div>