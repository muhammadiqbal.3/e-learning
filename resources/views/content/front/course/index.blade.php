@extends('layouts.public')

@section('content')
    @include('content.front.course.partials.breadcrumb', [
        'page_title' => 'Semua Kelas',
        'page_location' => 'Semua Kelas'
    ])

    <section class="padding-y-60 bg-light-v2">
        <div class="container">
            <div class="row">
                @if($course->isEmpty())
                    <div class="alert alert-warning text-center" style="width:100%" role="alert">
                        Maaf, Kami belum memiliki kelas terbaru!
                    </div>
                @else
                    @foreach($course as $item)
                    <div class="col-lg-4 col-md-6 marginTop-30">
                        <div href="page-course-details.html" class="card height-100p text-gray shadow-v1">
                            <img class="card-img-top" src="{{ url('assets/gambar_course/'.$item->course_thumbnail) }}" alt="{{ $item->course_name }}" style="width: 200px height: 200px;">
                            <div class="card-body">
                                <a href="{{ route('course.show', $item->course_name) }}" class="h5">
                                    {{ $item->course_name }}
                                </a>
                                <p class="mb-0">
                                    {{ $item->course_detail }}
                                </p>
                                <!-- <p class="my-3">
                                    <i class="ti-user mr-2"></i> Andrew Mead
                                </p> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
            <!-- END row-->
        </div>
        <!-- END container-->
    </section>
@endsection