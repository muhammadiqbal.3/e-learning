@extends('layouts.public')

@section('content')
    @include('content.front.blog.partials.breadcrumb', [
        'page_title' => 'Blog',
        'page_location' => 'Blog'
    ])

    <section class="pt-5 paddingBottom-100 bg-light-v2">
        <div class="container">
            <div class="row">
                @if($blog->isEmpty())
                    <div class="alert alert-warning text-center" style="width:100%" role="alert">
                        Maaf, Kami belum memiliki blog terbaru!
                    </div>
                @else
                    @foreach($blog as $b)
                    <div class="col-lg-4 col-md-6 marginTop-30">
                        <div class="card shadow-v1">
                            <img class="card-img-top" src="{{ url('assets/gambar_blog/'.$b->blog_thumbnail) }}" alt="">
                            <div class="card-body">
                                <a href="{{ route('blog.show', $b->blog_name) }}" class="h6 mb-3">
                                {{ $b->blog_name }}
                                </a>
                                <!-- <p class="mb-0">
                                    Investig ationes demons trave runt lectores legere liusry quod
                                </p> -->
                            </div>
                            <!-- <div class="card-footer">
                                <div class="media">
                                    <i class="ti-user"></i>
                                    <div class="media-body ml-4">
                                        Alex <br> 6 Oct, 2018 - in Web Developer
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
            <!--END row-->
        </div>
        <!-- END container-->
    </section>
@endsection