<div class="py-5 bg-cover text-white" data-dark-overlay="5" style="background:url('{{ asset('public_asset/assets/img/1920/658_2.jpg') }}') no-repeat">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2>{{ $page_title }}</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb justify-content-md-end bg-transparent">
                        <li class="breadcrumb-item">
                            <a href="{{ route('public.index') }}">Beranda</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('blog') }}">{{ $page_before }}</a>
                        </li>
                        <li class="breadcrumb-item">
                            {{ $page_location }}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>