<section class="pt-5 paddingBottom-150 bg-light-v2">
        <div class="container">
            <div class="row">
                <div class="">
                    <div class="card">
                        <img class="card-img-top" src="{{ url('assets/gambar_blog/'.$blog->blog_thumbnail) }}" alt="">

                        <div class="card-body">
                            <h2 class="my-4">
                                {{ $blog->blog_name }}
                            </h2>
                            <p class="" style="border-style: double;">
                                &nbsp;
                                <i class="ti-file"></i>
                                Informasi Terbaru &nbsp;&nbsp;
                                <i class="ti-alarm-clock"></i>
                                {{ date('M d, Y', strtotime($blog->created_at)) }}
                            </p>

                            <p>
                                {!! $blog->blog_detail !!}
                            </p>

                        </div>
                        <!-- END card-body-->
                    </div>
                    <!-- END card-->
                </div>
                <!-- END col-lg-9 -->
            </div>
            <!-- END row-->
            <div class="row mb-5 ">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 " style="top: 50px; ">
                    <center>
                        <a href="{{ route('blog') }}" class="btn btn-primary btn-lg mt-3 wow fadeIn ">Lihat Semua Blog</a>
                    </center>
                </div>
            </div>
        </div>
        <!-- END container-->
    </section>
    <!-- END section -->