@extends('layouts.public')

@section('content')
    @include('content.front.blog.partials.breadcrumb2')
    @include('content.front.blog.partials.content')

@endsection