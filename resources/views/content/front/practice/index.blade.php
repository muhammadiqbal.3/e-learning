@extends('layouts.public')

@section('content')
    @include('content.front.practice.partials.breadcrumb')

<section class="padding-y-100 bg-light-v4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-8">
                <div class="card padding-30 shadow-v1 marginTop-30">
                    @if(count($question) == 0)
                        <div class="alert alert-warning text-center" style="width:100%" role="alert">
                            Maaf, Kami Belum Memiliki Soal Latihan!
                        </div>
                        <center>
                            <a href="{{ route('lesson', $lesson->id) }}" class="btn btn-primary btn-lg mt-3 wow fadeIn">
                                Kembali Ke Materi
                            </a>
                        </center>
                    @elseif($lesson->status == 'pending')
                        <div class="alert alert-warning text-center" style="width:100%" role="alert">
                            Maaf, Soal Latihan Tertunda!
                        </div>
                        <center>
                            <a href="{{ route('lesson', $lesson->id) }}" class="btn btn-primary btn-lg mt-3 wow fadeIn">
                                Kembali Ke Materi
                            </a>
                        </center>
                    @else
                        <h2>Silahkan pilih jawaban yang paling benar menurut anda!</h2>
                        <form method="POST" action="{{ route('review', $lesson->id) }}">
                            {{ csrf_field() }}
                            <div class="mb-5">
                                <?php
                                    $quest = 0;
                                    $index = 0;
                                    foreach($question as $q_data){
                                ?>
                                    <br>
                                    
                                    <br>
                                    <h5>
                                        {{ $q_data->question }}
                                    </h5>
                                    <input type="hidden" name="question_id[{{$index}}]" value="{{$q_data->id}}">
                                        
                                    <?php
                                        $answer = DB::table('t_answer')
                                            ->where('question_id', $q_data->id)
                                            ->get();
                                            $i = 1;
                                            foreach($answer as $a_data){
                                    ?>

                                        <br>
                                        <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">
                                            <input type="radio" name="question{{ $index }}" id="answer{{$quest+$i}}" class="ec-checkbox__control ec-checkbox__label"> {{ $a_data->answer }}
                                            <input type="hidden" name="question_input{{ $quest+$i }}" id="answer_input{{$quest+$i}}" value="false">
                                        </label>

                                <?php 
                                $i++;    
                            }
                                $index++;
                                $i -= 1;
                                $quest = $index*$i;
                                    
                            } ?>
                            </div>
                            <center>
                                <button type="submit" class="btn btn-primary btn-lg mt-3">Selesai Mengerjakan</button>
                            </center>
                        </form>
                     @endif
                </div>
            </div>
        </div>
        <!-- END row-->
    </div>
    <!-- END container-->
</section>
@endsection

@section('inline_js')
<script>
    $("#answer1").change(function() {
        if(this.checked) {
            var i;
            for (i = 1; i <= 5; i++) {
                if(i == 1){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer2").change(function() {
        if(this.checked) {
            var i;
            for (i = 1; i <= 5; i++) {
                if(i == 2){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer3").change(function() {
        if(this.checked) {
            var i;
            for (i = 1; i <= 5; i++) {
                if(i == 3){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer4").change(function() {
        if(this.checked) {
            var i;
            for (i = 1; i <= 5; i++) {
                if(i == 4){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer5").change(function() {
        if(this.checked) {
            var i;
            for (i = 1; i <= 5; i++) {
                if(i == 5){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer6").change(function() {
        if(this.checked) {
            var i;
            for (i = 6; i <= 10; i++) {
                if(i == 6){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer7").change(function() {
        if(this.checked) {
            var i;
            for (i = 6; i <= 10; i++) {
                if(i == 7){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer8").change(function() {
        if(this.checked) {
            var i;
            for (i = 6; i <= 10; i++) {
                if(i == 8){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer9").change(function() {
        if(this.checked) {
            var i;
            for (i = 6; i <= 10; i++) {
                if(i == 9){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer10").change(function() {
        if(this.checked) {
            var i;
            for (i = 6; i <= 10; i++) {
                if(i == 10){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer11").change(function() {
        if(this.checked) {
            var i;
            for (i = 11; i <= 15; i++) {
                if(i == 11){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer12").change(function() {
        if(this.checked) {
            var i;
            for (i = 11; i <= 15; i++) {
                if(i == 12){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer13").change(function() {
        if(this.checked) {
            var i;
            for (i = 11; i <= 15; i++) {
                if(i == 13){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer14").change(function() {
        if(this.checked) {
            var i;
            for (i = 11; i <= 15; i++) {
                if(i == 14){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer15").change(function() {
        if(this.checked) {
            var i;
            for (i = 11; i <= 15; i++) {
                if(i == 15){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer16").change(function() {
        if(this.checked) {
            var i;
            for (i = 16; i <= 20; i++) {
                if(i == 16){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer17").change(function() {
        if(this.checked) {
            var i;
            for (i = 16; i <= 20; i++) {
                if(i == 17){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer18").change(function() {
        if(this.checked) {
            var i;
            for (i = 16; i <= 20; i++) {
                if(i == 18){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer19").change(function() {
        if(this.checked) {
            var i;
            for (i = 16; i <= 20; i++) {
                if(i == 19){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer20").change(function() {
        if(this.checked) {
            var i;
            for (i = 16; i <= 20; i++) {
                if(i == 20){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer21").change(function() {
        if(this.checked) {
            var i;
            for (i = 21; i <= 25; i++) {
                if(i == 21){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer22").change(function() {
        if(this.checked) {
            var i;
            for (i = 21; i <= 25; i++) {
                if(i == 22){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer23").change(function() {
        if(this.checked) {
            var i;
            for (i = 21; i <= 25; i++) {
                if(i == 23){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer24").change(function() {
        if(this.checked) {
            var i;
            for (i = 21; i <= 25; i++) {
                if(i == 24){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
    $("#answer25").change(function() {
        if(this.checked) {
            var i;
            for (i = 21; i <= 25; i++) {
                if(i == 25){
                    $( "#answer_input"+i).val( "true" );
                }else{
                    $( "#answer_input"+i).val( "false" );
                }
            }
        }
    });
</script>
@endsection
