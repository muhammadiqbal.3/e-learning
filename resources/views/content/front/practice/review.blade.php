@extends('layouts.public')

@section('content')
    @include('content.front.practice.partials.breadcrumb2')
    
    <section class="padding-y-100 bg-light-v4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8">
                    <div class="card padding-30 shadow-v1  marginTop-30 text-center">
                        <h4 class="mb-3">Hasil Latihan</h4>
                        <p> Total Benar : {{$question_true}} / 5 </p>
                        <p> Nilai : {{$score_akhir}}  </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-8">
                    <div class="card padding-30 shadow-v1  marginTop-30 text-center">
                        <h4 class="mb-3">Keterangan</h4>
                        <center>
                            <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">                                            
                                <input type="checkbox" name="checkbox" checked>
                                <span class="ec-checkbox__control"></span>
                                <span class="ec-checkbox__label">Jawaban Yang Benar</span>
                            </label>
                            <br>
                            <label class="ec-checkbox check-rounded check-danger check-xs mb-3 mr-4">                                            
                                <input type="checkbox" name="checkbox" checked>
                                <span class="ec-checkbox__control"></span>
                                <span class="ec-checkbox__label">Jawaban Yang Salah</span>
                            </label>
                        </center>
                    </div>
                </div>
                
                <!-- <div class="col-lg-6 col-md-4">
                    <div class="card padding-30 shadow-v1  marginTop-30 text-center">
                        <h4 class="mb-3">Review Soal</h4><br>
                        <ul class="pagination pagination-primary justify-content-center">
                            <li class="page-item active mx-1">
                                <a class="page-link iconbox iconbox-sm rounded-0" href="#">1</a>
                            </li>
                            <li class="page-item mx-1">
                                <a class="page-link iconbox iconbox-sm rounded-0" href="#">2</a>
                            </li>
                            <li class="page-item mx-1">
                                <a class="page-link iconbox iconbox-sm rounded-0" href="#">3</a>
                            </li>
                            <li class="page-item mx-1">
                                <a class="page-link iconbox iconbox-sm rounded-0" href="#">4</a>
                            </li>
                            <li class="page-item mx-1">
                                <a class="page-link iconbox iconbox-sm rounded-0" href="#">5</a>
                            </li>
                        </ul>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-8">
                    <div class="card padding-30 shadow-v1 marginTop-30">

                        <div class="mb-5">
                        <?php
                                    $quest = 0;
                                    $index = 0;
                                    foreach($question as $q_data){
                                ?>
                                    <br>
                                    
                                    <br>
                                    <h5>
                                        {{ $q_data->question }}
                                    </h5>
                                    
                                    <?php
                                        $answer = DB::table('t_answer')
                                            ->where('question_id', $q_data->id)
                                            ->get();
                                            $i = 1;
                                            foreach($answer as $a_data){
                                    ?>

                                        <br>
                                        @if($jawaban_user[$i] == "true" && $a_data->correct == "true")
                                            <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">                                            
                                                <input type="checkbox" name="checkbox" checked>
                                                <span class="ec-checkbox__control"></span>
                                                <span class="ec-checkbox__lebel" name="question{{ $index }}">{{ $a_data->answer }}</span>
                                            </label>
                                        @elseif($jawaban_user[$i] == "true" && $a_data->correct == "false")
                                        <label class="ec-checkbox check-rounded check-danger check-xs mb-3 mr-4">                                            
                                                <input type="checkbox" name="checkbox" checked>
                                                <span class="ec-checkbox__control"></span>
                                                <span class="ec-checkbox__lebel" name="question{{ $index }}">{{ $a_data->answer }}</span>
                                            </label>
                                        @elseif($jawaban_user[$i] == "false" && $a_data->correct == "true")
                                        <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">                                            
                                                <input type="checkbox" name="checkbox" checked>
                                                <span class="ec-checkbox__control"></span>
                                                <span class="ec-checkbox__lebel" name="question{{ $index }}">{{ $a_data->answer }}</span>
                                            </label>
                                        @else
                                        <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">                                            
                                                <input type="checkbox" name="checkbox">
                                                <span class="ec-checkbox__control"></span>
                                                <span class="ec-checkbox__lebel" name="question{{ $index }}">{{ $a_data->answer }}</span>
                                            </label>
                                        @endif
                                <?php 
                                $i++;     
                            }
                            $index++;
                            $i -= 1;
                            $quest = $index*$i;
                            } ?>
                            <!-- <h4>
                                Apakah itu HTML?
                            </h4>
                            <label class="ec-checkbox check-rounded check-danger check-xs mb-3 mr-4">
                              <input type="checkbox" name="checkbox" checked>
                              <span class="ec-checkbox__control"></span>
                              <span class="ec-checkbox__lebel">1</span>
                            </label>
                            <br>
                            <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">
                              <input type="checkbox" name="checkbox">
                              <span class="ec-checkbox__control"></span>
                              <span class="ec-checkbox__lebel">2</span>
                            </label>
                            <br>
                            <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">
                              <input type="checkbox" name="checkbox" checked>
                              <span class="ec-checkbox__control"></span>
                              <span class="ec-checkbox__lebel">3</span>
                            </label>
                            <br>
                            <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">
                              <input type="checkbox" name="checkbox">
                              <span class="ec-checkbox__control"></span>
                              <span class="ec-checkbox__lebel">4</span>
                            </label>
                            <br>
                            <label class="ec-checkbox check-rounded check-xs mb-3 mr-4">
                              <input type="checkbox" name="checkbox">
                              <span class="ec-checkbox__control"></span>
                              <span class="ec-checkbox__lebel">5</span>
                            </label> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END row-->
        </div>
        <!-- END container-->
        <div class="row mb-5 ">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 " style="top: 50px; ">
                <center>
                    <a href="{{ route('public.index') }}" class="btn btn-primary btn-lg mt-3 wow fadeIn ">KEMBALI KE BERANDA</a>
                </center>
            </div>
        </div>
    </section>

@endsection