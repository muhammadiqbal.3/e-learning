<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Front
Route::get('/', 'front\FrontController@index')->name('public.index');
// Front - Course
Route::get('/course', 'front\CourseController@index')->name('course');
Route::get('/course/{name}', 'front\CourseController@show')->name('course.show');
// Front - Lesson
// Route::get('/course/{course_name}/{lesson_name}', 'front\LessonController@index')->name('lesson');
Route::get('/course/lesson/{lesson_name}', 'front\LessonController@index')->name('lesson');
// Front - Practice
Route::get('/course/lesson/{lesson_id}/exercise', 'front\PracticeController@index')->name('practice');
Route::post('/course/lesson/{lesson_id}/review', 'front\ReviewController@index')->name('review');
//Front - Blog
Route::get('/blog', 'front\BlogController@index')->name('blog');
Route::get('/blog/{blog_name}', 'front\BlogController@show')->name('blog.show');
//Front - Profile
// Route::get('/profile', 'ProfileController@index')->name('profile');
// Route::get('/profile', 'ProfileController@update')->name('profile');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile/{id}', 'ProfileController@update')->name('profile.update');
Route::put('/profile/{id}/password', 'ProfileController@updatePassword')->name('profile.password');

// //Dashboard - Blog
// Route::get('dashboard/blog/edit/{id}', 'dashboard\BlogController@edit');
// //Dashboard - Course
// Route::get('dashboard/course/edit/{id}', 'dashboard\CourseController@edit');
// //Dashboard - Lesson
// Route::get('dashboard/lesson/edit/{id}', 'dashboard\LessonController@edit');

Route::group([
    'prefix' => 'dashboard',
    'middleware' => ['web', 'auth']
], function (){
    Route::get('/', 'dashboard\DashboardController@index')->name('dashboard');

    Route::resource('/course', 'dashboard\CourseController', ['as' => 'dashboard']);

    Route::resource('/lesson', 'dashboard\LessonController', ['as' => 'dashboard']);

    Route::resource('/practice', 'dashboard\PracticeController', ['as' => 'dashboard']);

    Route::resource('/blog', 'dashboard\BlogController', ['as' => 'dashboard']);

    Route::resource('/student', 'dashboard\StudentController', ['as' => 'dashboard']);
});

Route::group([
    'prefix' => 'datatable',
    'middleware' => ['web', 'auth']
], function(){
    Route::get('/course', 'dashboard\CourseController@datatableIndex')->name('datatable.course.index');
    Route::get('/lesson', 'dashboard\LessonController@datatableIndex')->name('datatable.lesson.index');
    Route::get('/practice', 'dashboard\PracticeController@datatableIndex')->name('datatable.practice.index');
    Route::get('/blog', 'dashboard\BlogController@datatableIndex')->name('datatable.blog.index');
    Route::get('/student', 'dashboard\StudentController@datatableIndex')->name('datatable.student.index');
});